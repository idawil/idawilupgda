package xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class ParseXML {
	

	public static Document parseFile(String fileName) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new File(fileName));
		doc.getDocumentElement().normalize();
		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		return doc;
	}

	public static String getText(Element element, String tagName) {
		return element.getElementsByTagName(tagName).item(0).getTextContent();
	}

	public static Document newDocument() throws ParserConfigurationException {
		DocumentBuilderFactory dbFactory =
		 DocumentBuilderFactory.newInstance();
		 DocumentBuilder dBuilder = 
		    dbFactory.newDocumentBuilder();
		 Document doc = dBuilder.newDocument();
		return doc;
	}

	public static void saveFile(Document doc, String FILE_NAME)
			throws TransformerFactoryConfigurationError, TransformerConfigurationException, TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(FILE_NAME));
		transformer.transform(source, result);
		// Output to console for testing
		StreamResult consoleResult = new StreamResult(System.out);
		transformer.transform(source, consoleResult);
	}

	public static void setAttribute(Document doc, Element element, String name, String value) {
		Attr attr = doc.createAttribute(name);
		 attr.setValue(value);
		 element.setAttributeNode(attr);
	}
}