package Office;

/**
 * Created by RENT on 2017-06-29.
 */
public class Client {
    private String pesel;
    private String typ_sprawy;

    public Client(String pesel, String typ_sprawy) {
        this.pesel = pesel;
        this.typ_sprawy = typ_sprawy;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getTyp_sprawy() {
        return typ_sprawy;
    }

    public void setTyp_sprawy(String typ_sprawy) {
        this.typ_sprawy = typ_sprawy;
    }
}
