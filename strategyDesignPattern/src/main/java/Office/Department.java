package Office;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-06-29.
 */
public class Department {
    private List<Office> offices=new LinkedList<>();

    public void addOffice(int pesel, Menu typSprawy){

        Client client=new Client();
        client.setPesel(pesel);
        client.setTyp_sprawy(typSprawy);
        offices.add(client);

    }

    public Office getOffice(int index){

        if (index>0&&index<=offices.size()){
            return  offices.get(index-1);
    }else {
        throw new IllegalArgumentException("Bad OfficeNumber");}
    }

}
