package bikes;

/**
 * Created by RENT on 2017-06-29.
 */
class Bike implements IBikeFactory{
    private String brand;
    private int seatsNumber;
    private int gears;
    private  BIKE_TYPE bikeType;



    public Bike(String brand, int seatsNumber, int gears, BIKE_TYPE bikeType) {
        this.brand = brand;
        this.seatsNumber = seatsNumber;
        this.gears = gears;
        this.bikeType = bikeType;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public void setSeatsNumber(int seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    public int getGears() {
        return gears;
    }

    public void setGears(int gears) {
        this.gears = gears;
    }

    public BIKE_TYPE getBikeType() {
        return bikeType;
    }

    public void setBikeType(BIKE_TYPE bikeType) {
        this.bikeType = bikeType;
    }


    @Override
    public String toString() {
        return "Bike{" +
                "brand='" + brand + '\'' +
                ", seatsNumber=" + seatsNumber +
                ", gears=" + gears +
                ", bikeType=" + bikeType +
                '}';
    }
}
