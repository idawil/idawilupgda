package bikes;

/**
 * Created by RENT on 2017-06-29.
 */
public abstract class BikeFactory {
    public static Bike create5GearKrossBike(){
        return new Bike("KROSS",1,5,BIKE_TYPE.BICYCLE);
    }
    public static Bike create6MeridaBike(){
        return new Bike("Merida",1,6,BIKE_TYPE.BICYCLE);

    }

    public static Bike create3GearInianaBike(){
        return new Bike("INIANA",2,3,BIKE_TYPE.TANDEM);
    }

    public static Bike create6GearFeltBike(){
        return new Bike("FELT",1,3,BIKE_TYPE.BICYCLE);
    }

    public static Bike create1GoetzeFeltBike(){
        return new Bike("GOETZE",2,3,BIKE_TYPE.TANDEM);
    }



}
