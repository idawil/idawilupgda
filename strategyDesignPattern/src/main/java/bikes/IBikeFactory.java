package bikes;

/**
 * Created by RENT on 2017-06-29.
 */
public interface IBikeFactory {
    int getGears();
    int getSeatsNumber();

}
