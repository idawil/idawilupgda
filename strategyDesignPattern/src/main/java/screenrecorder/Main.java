package screenrecorder;

import java.util.Scanner;

/**
 * Created by RENT on 2017-06-29.
 */
public class Main {
    public static void main(String[] args) {

        ScreenRecorder screenRecorder = new ScreenRecorder();
        Scanner sc = new Scanner(System.in);
        boolean exit = false;
        while (!exit) {
            System.out.println("Co chcesz zrobić? 1.add,  2. set,  3. list, 4. start recording," +
                    " 5. stop recording, 6.zakończ program");

            int x = Integer.parseInt(sc.nextLine());

            switch (x) {
                case (1):
                    screenRecorder.addProfile(createProfile());
                    break;
                case (2):
                    System.out.println("podaj index profilu");
                    int actualProfile = Integer.parseInt(sc.nextLine());
                    screenRecorder.setProfile(actualProfile);
                    break;
                case (3):
                    screenRecorder.listProfiles();
                    break;
                case (4):
                    screenRecorder.startRecording();
                    break;
                case (5):
                    screenRecorder.stopRecording();
                    break;
                case (6):
                    exit = true;
                    break;
            }
        }
    }

    public static Profile createProfile() {
        Scanner sc = new Scanner(System.in);
        System.out.println("CodecType: ");
        String codecType = sc.nextLine();
        //konwertuje string na enum
        Codec codec = Codec.valueOf(codecType);
        System.out.println("screenrecorder.Extension: ");
        String extensionType = sc.nextLine();
        Extension extension = Extension.valueOf(extensionType);
        System.out.println("screenrecorder.Resolution: ");
        String resolutionType = sc.nextLine();
        Resolution resolution = Resolution.valueOf(resolutionType);
        return new Profile(codec, extension, resolution);
    }
}
