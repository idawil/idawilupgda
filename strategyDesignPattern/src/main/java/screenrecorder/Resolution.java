package screenrecorder;

/**
 * Created by RENT on 2017-06-29.
 */
public enum Resolution {
    R800x600, R1280x720, R1920x1080, R3840x2160;
}
