package screenrecorder;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-06-29.
 */
public class ScreenRecorder {

    private List<Profile> profils = new LinkedList<>();
    private Profile actualProfile = null;
    private boolean isRecording = false;


    //dodaje profil do listy
    public void addProfile(Profile nowyProfil) {
        profils.add(nowyProfil);
    }

    //ustawia profil (pole) na wybrany z listy
    public void setProfile(int numerProfilu) {
        if (numerProfilu < profils.size()) {
            this.actualProfile = profils.get(numerProfilu);
        } else {
            throw new IllegalArgumentException("Profil o tym indeksie nie istnieje");
        }

    }

    //wylistowuje profile
    public void listProfiles() {
        if (profils.size() != 0) {
            for (int i = 0; i < profils.size(); i++) {
                System.out.println(i + " " + profils.get(i));
            }
        } else {
            System.out.println("Lista profili jest pusta");
        }
    }

    //rozpoczyna nagrywanie (tylko gdy jeszcze nie nagrywamy)
    public void startRecording() {
        if (actualProfile != null && isRecording == false) {
            System.out.println("Start recording");
            System.out.println(actualProfile);
            isRecording = true;
        } else if (isRecording == true) {
            System.out.println("Nagrywanie trwa");

        } else {
            System.out.println("Ustaw profil!");
        }
    }

    //zatrzymuje nagrywanie (tylko gdy już nagrywamy)
    public void stopRecording() {
        if (isRecording == true) {
            System.out.println("Stop recording");
            isRecording = false;
        } else {
            System.out.println("Nagrywanie nie było rozpoczęte");
        }
    }
}
