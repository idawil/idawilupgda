package pl.org.pfig.ida.main;


import pl.org.pfig.ida.time.MyTime;

public class Main {
	public static void main(String[] args) {
		MyTime mt = new MyTime(23, 59, 59);
		MyTime mt2 = mt.nextSecond();
		MyTime mt3=mt.previousSecond();
		MyTime mt4=mt.nextMinute();
		MyTime mt5=mt.previousMinute();
		MyTime mt6=mt.nextHour();
		MyTime mt7=mt.previousHour();
		
		System.out.println(mt);
		System.out.println(mt2);
		System.out.println(mt3);
		System.out.println(mt4);
		System.out.println(mt5);
		System.out.println(mt6);
		System.out.println(mt7);
		
	}
}
