package pl.org.pfig.main;

public class StringUtil {
	private String str;

	public StringUtil(String str) {
		this.str = str;
	}

	public StringUtil print() {
		System.out.println(str);
		return this;
	}

	public StringUtil prepend(String arg) {
		str = arg + str;
		return this;
	}

	public StringUtil append(String arg) {
		str = str + arg;
		return this;
	}

	public StringUtil letterSpacing() {
		String[] letters = str.split("");
		str = "";
		for (String letter : letters) {
			str += letter + " ";
		}
		str = str.substring(0, str.length() - 1);
		return this;
	}

	public StringUtil reverse(){
		String[] letters=str.split("");
		str="";
		for(int i=letters.length-1;i>=0;i--){
			str+=letters[i];
		}
		
					
		return this;
	}
	
	public StringUtil getAlphabet(){
		str="";
		for(char c = 97; c <= 122; c++) {
			str += c;
		}
		return this;
	}
	
	public StringUtil getFirstLetter(){
		str=str.charAt(0)+"";
		return this;
	}
	
	public StringUtil limit (int n){
		str=str.substring(0,n);
		return this;
	}

	public StringUtil insertAt(String string, int n){
		str=str.substring(0,n)+ string + str.substring(n);
		return this;
	}
	
	public StringUtil resetText(){
		str="";
		return this;
	};
	
	public StringUtil swapLetters(){
		str=str.substring(str.length()-1,str.length()) + str.substring(1, str.length()-1) + str.substring(0, 1);
		return this;
	}
	

}
