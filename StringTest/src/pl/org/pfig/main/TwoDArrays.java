package pl.org.pfig.main;

public class TwoDArrays {
	public int addElementsDividedBySeven(int[][] arr){
		int sum=0;
		int rows=arr.length;
		for(int i=0;i<rows;i++){
			int cols=arr[i].length;
			for(int j=0; j<cols;j++){
				if(arr[i][j]%7==0){
					sum+=arr[i][j];
				}
			}
		}
		return sum;
	}
	public int multiplyElements(int[][] arr){
		int multi=1;
		int rows=arr.length;
		for(int i=0;i<rows;i++){
			int cols=arr[i].length;
			for(int j=0; j<cols;j++){
				
					multi*=arr[i][j];
							}
		}
		return multi;
	}
	
	public int multiplyEvenElements(int[][] arr){
		int multi=1;
		int rows=arr.length;
		for(int i=0;i<rows;i++){
			int cols=arr[i].length;
			for(int j=0; j<cols;j++){
				if(arr[i][j]%2==0){
					multi*=arr[i][j];
				}
			}
		}
		return multi;
		
	}
	
	public int multiplyOddElements(int[][] arr){
		int multi=1;
		int rows=arr.length;
		for(int i=0;i<rows;i++){
			int cols=arr[i].length;
			for(int j=0; j<cols;j++){
				if(arr[i][j]%2==1){
					multi*=arr[i][j];
				}
				
			}
		}
		if(multi == 1) multi = 0;
		return multi;
		
	}
	
	public int multiplyElementsDividedByThree(int[][] arr){
		int multi=1;
		int rows=arr.length;
		for(int i=0;i<rows;i++){
			int cols=arr[i].length;
			for(int j=0; j<cols;j++){
				if(arr[i][j]%3==0){
					multi*=arr[i][j];
				}				
			}
		}
		return multi;
	}
	
	public int find2DArrayMin(int[][] arr){
		int min=arr[0][0];
		int rows=arr.length;
		for(int i=0;i<rows;i++){
			int cols=arr[i].length;
			for(int j=0; j<cols;j++){
				if(arr[i][j]< min){
					min=arr[i][j];
				}
			}
		}
		return min;
	}
	
	public int find2DArrayMax(int[][] arr){
		int max=arr[0][0];
		int rows=arr.length;
		for(int i=0;i<rows;i++){
			int cols=arr[i].length;
			for(int j=0; j<cols;j++){
				if(arr[i][j]> max){
					max=arr[i][j];
				}
			}
		}
		return max;
	}
	
	
	
}
