package contact;

/**
 * Created by RENT on 2017-07-04.
 */
public class Contact {
    private final String firstName;
    private final String lastName;
    private final String number;
    private final String eMail;


    public Contact(String firstName, String lastName, String number, String eMail) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.number = number;
        this.eMail = eMail;
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getNumber() {
        return number;
    }

    public String geteMail() {
        return eMail;
    }
}
