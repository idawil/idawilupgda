package instrumenty;

public abstract class InstrumentsMember {

	private String name;
	private int cost;
	private String color;
	
	public InstrumentsMember(String name,int cost, String color){
		this.name=name;
		this.cost=cost;
		this.color=color;
	}
	public String getName() {
		return name;
	}
	public int getCost() {
		return cost;
	}
	public String getColor() {
		return color;
	}
	
	public abstract void introduce();
	
};