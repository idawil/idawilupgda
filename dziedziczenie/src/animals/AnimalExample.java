package animals;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class AnimalExample {

	public static void main(String[] args) {

		Animal[] animals = { new Cat(), new Dog() };
		for (Animal animal : animals) {
			System.out.println(animal.makeNoise());
		}

	}

	public static void save5NoisesToFile(String filename, Animal animal) {
		try {
			PrintStream out = new PrintStream(filename);
			out.println(animal.makeNoise());
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
