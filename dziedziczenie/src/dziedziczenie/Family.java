package dziedziczenie;

public class Family {
	
	public static void main(String[] args){
		
		Mother mother= new Mother("Gra�yna");
		introduce(mother);
		Father father= new Father("Janusz");
		introduce(father);
		Son son= new Son("Adam");
		introduce(son);
		Daughter daughter= new Daughter("Ania");
		introduce(daughter);
		
	}
	
	public static void introduce(Mother mother){
	System.out.println("I�m a mother. My name is " + mother.getName());
	}
	public static void introduce(Father father){
		System.out.println("I�m a father. My name is " +father.getName());
		}
	public static void introduce(Son son){
		System.out.println("I�m a son. My name is " + son.getName());
		}
	public static void introduce(Daughter daughter){
		System.out.println("I�m a daughter. My name is " + daughter.getName());
		}
}
