package family.dziedziczenie;

public class Family {

	public static void main(String[] args) {

		FamilyMember mother = new Mother("Gra�yna");
		FamilyMember father = new Father("Janusz");
		FamilyMember son = new Son("Adam");
		FamilyMember daughter = new Daughter("Ania");
		
		FamilyMember[] members={mother, father, daughter, son};
		
		for (FamilyMember familyMember : members) {
			familyMember.introduce();
			
		}
		
		/*mother.introduce();		
		father.introduce();	
		son.introduce();		
		daughter.introduce();*/

	}

}
