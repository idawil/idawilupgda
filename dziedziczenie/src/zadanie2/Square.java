package zadanie2;

public class Square implements Figure{
private double a;
private String name;
public Square(double a){
	this.a=a;
	this.name="kwadrat o boku d�ugo�ci "+ a;
}
public String getName() {
	return name;
}

public double countArea(){
	return a*a;
}
public double countCircumference(){
	return 4*a;
}
}
