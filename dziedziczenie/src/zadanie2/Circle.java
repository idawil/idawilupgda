package zadanie2;

public class Circle implements Figure {
	private double r;
	private String name;

	public Circle(double r) {
		this.r = r;
		this.name="Ko�o o promieniu d�ugo�ci "+ r;
	}
	
	public String getName() {
		return name;
	}

	public double countArea() {
		return r * r * 3.14;
	}

	public double countCircumference() {
		return 2 * 3.14 * r;
	}

}
