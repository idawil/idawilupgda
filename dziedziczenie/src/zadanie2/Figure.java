package zadanie2;

public interface Figure {
	String getName();
	double countArea();
	double countCircumference();

}
