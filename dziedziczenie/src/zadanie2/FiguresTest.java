package zadanie2;

public class FiguresTest {

	public static void main(String[] args) {
		Figure[] figures = { new Circle(2.0), new Square(1.0), new Triangle(1.0, 2.0, 2.0) };
		for (Figure figure : figures) {
			show(figure);
		}
	}

	private static void show(Figure figure) {
		System.out.println(
				figure.getName() + " ma pole: " + figure.countArea() 
				+ " i obw�d " + figure.countCircumference());
	}

}
