package zadanie2;

public class Triangle implements Figure {
	private double a; 
	private double b; 
	private double c;
	
	private String name;

	public Triangle(double a, double b, double c) {
		this.a=a;
		this.b=b;
		this.c=c;
		
		this.name="Tr�jk�t o bokach "+ a +" "+b+" "+c+" ";
	}
		
	public String getName() {
		return name;
	}
	public double countArea(){
		double p= (a+b+c)/2;
		return Math.sqrt(p*(p-a)*(p-b)*(p-c));
	}
	public double countCircumference(){
		return a+b+c;
	}
}
