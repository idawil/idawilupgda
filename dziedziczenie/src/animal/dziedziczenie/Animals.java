package animal.dziedziczenie;

public class Animals {

	public static void main(String[] args) {
		AnimalMember cat=new Cat("Filemon","Garden");
		AnimalMember dog= new Dog("Burek","Buda");
		AnimalMember krowa= new Krowa("Kamila","Obora");
		AnimalMember fish= new Fish("Dorota","Water");
		
		
		AnimalMember[] members={cat, dog, krowa,fish};
		for (AnimalMember animalMember : members) {
			animalMember.introduce();
			animalMember.myhome();
		}
		
	}

}
