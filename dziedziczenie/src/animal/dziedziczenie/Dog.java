package animal.dziedziczenie;

public class Dog extends AnimalMember{
	
	public Dog(String name,String home){
		super(name,home);
		
	}
	
	@Override
	public void introduce(){
		System.out.println("I'm a dog, My name is :"+ this.getName());
	}
	@Override
	public void myhome() {
		System.out.println("Mieszkam w "+ this.getHome());
		
	}

}
