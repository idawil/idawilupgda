package animal.dziedziczenie;

public abstract class AnimalMember {
	private String name;
	private String home;
	
	public AnimalMember(String name, String home){
		this.name=name;
		this.home=home;
	}
	
	
	public String getName(){
		return name;
	}
	public abstract void introduce();
	
	

	
	public String getHome(){
		return home;
	}
	
	public abstract void myhome();
}
