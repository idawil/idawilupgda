package animal.dziedziczenie;

public class Cat extends AnimalMember{
	public Cat(String name,String home){
		super(name,home);
		
	}
	@Override
	public void introduce(){
		System.out.println("I'm a Cat. My name is: "+ this.getName());
	}
	@Override
	public void myhome() {
		System.out.println("Mieszkam w "+ this.getHome());
		
	}
}

