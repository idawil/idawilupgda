package animal.dziedziczenie;

public class Fish extends AnimalMember {
	public Fish(String name,String home){
		super(name,home);
		
		}

	@Override
	public void introduce() {
		System.out.println("I'm a Fish: "+ this.getName());
		
	}
	@Override
	public void myhome() {
		System.out.println("Mieszkam w "+ this.getHome());
		
	}
	
}
