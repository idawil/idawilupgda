package pl.sdacademy.moviedb;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sdacademy.moviedb.entity.Genre;
import pl.sdacademy.moviedb.entity.Movie;
import pl.sdacademy.moviedb.util.HibernateUtil;

import java.util.Scanner;

/**
 * Created by RENT on 2017-07-13.
 */
public class MovieDB {
    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);

        boolean exit=false;
        while(!exit) {

            System.out.println("1.ADD MOVIE, 2.ADD GENRE, 3.EXIT");
            int x = Integer.parseInt(sc.nextLine());

            switch (x) {
                case (1):
                    System.out.println("title:");
                    String title = sc.nextLine();
                    System.out.println("year");
                    int year = Integer.parseInt(sc.nextLine());
                    System.out.println("duration");
                    double duration = Double.parseDouble(sc.nextLine());
                    System.out.println("description");
                    String description = sc.nextLine();
                    System.out.println("genre");
                    String genre = sc.nextLine();

                    Session session= HibernateUtil.openSession();
                    Transaction t=session.beginTransaction();

                    session.save(new Movie(title,year,duration,description,new Genre(genre)));
                    t.commit();
                    session.close();
                    break;

                case (2):
                    System.out.println("name:");
                    String name = sc.nextLine();
                    Session s= HibernateUtil.openSession();
                    Transaction transaction=s.beginTransaction();

                    s.save(new Genre(name));
                    transaction.commit();
                    s.close();
                    break;
                case (3):
                    exit = true;
                    sc.close();
                    break;
            }
        }








    }
}
