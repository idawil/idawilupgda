package pl.sdacademy.moviedb.crud;

import java.util.List;

/**
 * Created by RENT on 2017-07-13.
 */
public class SimpleCRUD<T> implements CRUD<T> {
    private Class<T> clazz;

    public  SimpleCRUD(Class<T> clazz){
        this.clazz=clazz;
    }


    public SimpleCRUD(T obj) {

    }


    public void insertOrUpdate(T obj) {

    }

    public void delete(T obj) {

    }

    public T select(int id) {
        return null;
    }

    public List<T> select() {
        return null;
    }
}
