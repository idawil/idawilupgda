package pl.sdacademy.moviedb.crud;

import java.util.List;

/**
 * Created by RENT on 2017-07-13.
 */
public interface CRUD<T> {
    void insertOrUpdate(T obj);
    void delete(T obj);
    T select(int id);
    List<T> select();

}
