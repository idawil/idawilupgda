package pl.sdacademy.moviedb.entity;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by RENT on 2017-07-13.
 */

@Entity
@Table(name= "genre")
public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private int id;
    @Column(name = "name")
    private String name;
    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL, mappedBy = "genre")

    private List<Movie> movies=new LinkedList<Movie>();

    public Genre(){

    }
    public Genre(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
