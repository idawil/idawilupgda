import java.util.Random;

/**
 * Created by RENT on 2017-06-26.
 */
public class MyMath {
    public static int sum(int a, int b){//0(1)
        return a+b;
    }
    public static long pow(int a, int b){//o(n)
        long result=1;
        for(int i=0;i<b;i++ ){
            result*=a;
        }
        return result;
    }

    public static int[] randArray (int size){//o(n)
        int[] result=new int[size];
        Random random=new Random();

        for(int i=0;i<size;i++ ){
            result[i]=random.nextInt();
        }
        return result;
    }

    public static int[][] randArrayTwoDim (int size){//o(n)
        int[][] result=new int[size][];


        for(int i=0;i<size;i++ ){//o(n^2)
            result[i]=randArray(size);
        }
        return result;
    }

    public static int find(int[] data, int value){//o(n/2)
        for(int i=0;i<data.length;i++){
            if(data[i]==value){
                return i;
            }
        }
        return -1;
    }


    public static int findBisection(int[] data, int value){//o(n/2)
            int a=0;
            int b=data.length;
            int cut;
            while (a<b){
                cut=(a+b)/2;
                if (data[cut]==value){
                    return cut;
                }else if(data[cut]<value){
                    a=cut+1;
                }else {b=cut-1;}
            }
            return -1;
    }
}
