import static org.junit.Assert.*;

/**
 * Created by RENT on 2017-06-26.
 */
public class MyMathTest {

    int[]data={1,3,4,5,6,7,8};


    @org.junit.Test
    public void findBisection() throws Exception {
        assert MyMath.findBisection(data,5)==3;
    }
    @org.junit.Test
    public void findBisectionNotExist() throws Exception {
        assert MyMath.findBisection(data,10)==-1;
    }

}