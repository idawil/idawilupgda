package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

public class AvgChecker {
	
	private String filename;

	public AvgChecker(String filename) {

		this.filename = filename;

	}
	
	
	public void process() {
		File f = new File(filename);
		double avg = 0, avgs=0, marksSum=0, num = 0;
		LinkedList<String> dd = new LinkedList<>();
		try {
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()) {
				String currentLine = sc.nextLine();
				
				if(!currentLine.equals("")){
					dd.add(currentLine);
				String[] data = currentLine.split("\t");
				marksSum=Double.parseDouble(data[1]) + Double.parseDouble(data[2]) + Double.parseDouble(data[3]);
				avgs=marksSum/(data.length);
				avg += marksSum;
				num += 3;
				}
			}
			avg /= num;
			System.out.println("Srednia wszystkich ocen to: " + avg);
			sc.close();
			
			/*for (String string : dd) {
				System.out.println(dd);
			}*/
		} catch (FileNotFoundException e) {

			System.out.println(e.getMessage());
		}

	}
	
	

}
