package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.lang.invoke.WrongMethodTypeException;
import java.util.LinkedList;
import java.util.Scanner;

public class Columner {
	
	private final String path = "resources/";
	private String filename;
	private double[] currentColumn;
	private LinkedList<String> fileContent = new LinkedList<>();
	
	
	public Columner(){}
	public Columner(String filename){
		this.filename = filename;
	}
	
	public double sumColumn(int column) throws  WrongColumnException{
		
		double sum=0;
		for(double d: readColumn(column)){
			sum+=d;
		}
		return sum;
	}


	public double avgColumn(int column) throws WrongColumnException{
		double avg=sumColumn(column)/fileContent.size();
		return avg;
		
	}
	
	
	
	public int countColumn(int column){
		readFile();
		return fileContent.get(0).split("\t").length;
	}
	
	
	
	public double maxColumn(int column) throws WrongColumnException{
		
		double max=readColumn(column)[0] ;
		for (int i = 1; i <readColumn(column).length; i++) {
			
//			if (readColumn(column)[i] > max){ 
//			    max = readColumn(column)[i] ; 
//			}
			max=Math.max(max,readColumn(column)[i]);
			  }

			return max;
	
		
	}

public double minColumn(int column) throws WrongColumnException{
	
	double min=readColumn(column)[0] ;
	for (int i = 1; i <readColumn(column).length; i++) {
		
//		if (readColumn(column)[i] < min){ 
//		    min = readColumn(column)[i] ; 
//		}
		min=Math.min(min, readColumn(column)[i]);
		  }
	return min;
	
	
}
	public void readFile() {

		if (fileContent.size() == 0) {
			File f = new File(path + filename);
			try {
				Scanner sc = new Scanner(f);
				while (sc.hasNextLine()) {
					fileContent.add(sc.nextLine());
				}
				sc.close();
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}
		}

	}
	
	public void writeColumn(String filename, int column) throws FileNotFoundException, WrongColumnException{
		FileOutputStream fos= new FileOutputStream(path+filename);
		PrintWriter pw= new PrintWriter(fos);
		double[] rc=readColumn(column);
		
		for (int i=0;i<rc.length;i++) {
			pw.println(rc[i]);
		}
		pw.close();
	
	}
	
	
	public double[] readColumn(int column) throws WrongColumnException{
			readFile();
			int maxColNum=fileContent.get(0).split("\t").length;
			if(column>=1&& column<=maxColNum){
				currentColumn =new double[fileContent.size()];
				int i=0;
				for(String line: fileContent){
					String[] cols=line.split("\t");
					currentColumn[i++]=Double.parseDouble(cols[column-1].replace(",", "."));
				}
				
			}else{
				throw new WrongColumnException("Bad column value");
			}
			return currentColumn;
		
	}
	
}
