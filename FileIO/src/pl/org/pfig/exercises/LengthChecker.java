package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {
	private final String path = "resources/";

	
	
	public void make(String fileInput, int len){
		
		
		String[] fileContent= readFile(fileInput);
			
		writeFile(fileContent, len);
		
	}
		
		
	
	private void writeFile(String[] fileContent, int len){
		
		File f=new File("resources/"+"words_"+len+".txt");
		try {
			FileOutputStream fos= new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			
			for (String s : fileContent){
				if(isProperLength(s, len)){			
				pw.println(s);
				}
			}
				
						
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		
	}
	
		
	private boolean isProperLength(String arg, int len){
		
		return arg.length()>len;
	}
	
	
	
	private String[] readFile(String filename) {
		File f = new File(path + filename);
		String[] ret = new String[countlines(filename)];

		try {
			Scanner sc = new Scanner(f);

			int i = 0;

			while (sc.hasNextLine()) {

				ret[i++] = sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		return ret;

	}
	
	
	private int countlines(String filename){
		File f=new File(path+filename);
		int lines=0;
		try {
			Scanner sc=new Scanner(f);
			while(sc.hasNextLine()){
				lines++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
		
			System.out.println(e.getMessage());
		}
		return lines;
	}
		
	

}
