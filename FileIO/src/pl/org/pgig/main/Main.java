package pl.org.pgig.main;

import java.io.FileNotFoundException;
import java.lang.invoke.WrongMethodTypeException;
import java.util.Arrays;
import java.util.List;

import pl.org.pfig.exercises.AvgChecker;
import pl.org.pfig.exercises.AvgChecker2;
import pl.org.pfig.exercises.Columner;
import pl.org.pfig.exercises.LengthChecker;
import pl.org.pfig.exercises.MoneyConverter;
import pl.org.pfig.exercises.Sentence;
import pl.org.pfig.exercises.Sentencer;
import pl.org.pfig.exercises.TempConverter;
import pl.org.pfig.exercises.WrongColumnException;

public class Main {

	public static void main(String[] args) throws FileNotFoundException, WrongColumnException {
		
		Columner c = new Columner("data.txt");
		c.writeColumn("writetest3.txt", 6);
		
		try {
			System.out.println(c.maxColumn(1));

		} catch (WrongColumnException e) {
			System.out.println(e.getMessage());
		}
		c.readFile();
		
		//AvgChecker2 avc= new AvgChecker2("marks.txt");
		//avc.process();
		
		
		//Student s1= new Student(123,"Pawel","Test", "programowanie",4.12);
		//Student s2= new Student(124,"Pawel","Testowy", "garncarstwo",3.12);
		
		//HashMap<Integer,Student> students=new HashMap<>();
		
		/*LengthChecker lc=new LengthChecker();
		lc.make("words.txt", 9);
		
		
		
		
		TempConverter tc= new TempConverter();
		double[] tempC=tc.readTemp("tempC.txt");
		for (double d : tempC) {
			System.out.println("Temp: "+d);
		}
		tc.writeTemp(tempC);
		
		
		
		
		MoneyConverter mc= new MoneyConverter();*/
		/*List<String> currencies = Arrays.asList("USD","EUR","JPY");
		for (String c : currencies) {
			System.out.println(c+" | "+ mc.readCourse(c));
		}*/
		
		//System.out.println(mc.convert(100, "USD"));
		
		
		/*Sentencer s=new Sentencer();
		String mySentence=s.readSentence("test.txt");
		s.writeSentence("mySentence.txt", mySentence);
		
		
		String myStr="1 USD   4,1827";
		String[] temp=myStr.split("\t");
		String[] values =temp[0].split(" ");
		
		int howMany=Integer.parseInt(values[0]);
		String currency=values[1];
		double course=Double.parseDouble(temp[1].replace(",","."));
		
		System.out.println(howMany+ " | "+ currency +" | "+ course);*/
	}

}
