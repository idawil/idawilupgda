package pl.org.pfig.cities.resolver;

import pl.org.pfig.cities.connector.DatabaseConnector;

import pl.org.pfig.cities.helper.QueryHelper;
import pl.org.pfig.cities.model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by RENT on 2017-07-06.
 */
public class StudentResolver extends AbstractResolver {

    private final String url="jdbc:mysql://localhost:3306/students";
    private final String user="root";
    private final String pass="";


    @Override
    public Object get(int id) {
        Student student = null;
        try {
            Connection c = DatabaseConnector.getInstance().getConnection();


            String query="SELECT * FROM `student` WHERE `id` = ?";
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1,id);



            ResultSet rs = ps.executeQuery();
            rs.next();



            student=new Student(rs.getInt("id"),rs.getString("name"),
                            rs.getString("lastname"));

            rs.close();
            ps.close();
            ps.close();
            return student;


        }catch(SQLException e){
            System.out.println("Student o tym id nie istnieje");
        }

        return student;

    }

    @Override
    public List get() {

        List<Student> listOfStudents=new ArrayList<>();
        try {
            Connection connection = DatabaseConnector.getInstance().getConnection();
            String query="SELECT * FROM `student`";

            Statement s= connection.createStatement();
            ResultSet rs=s.executeQuery(query);

            while (rs.next()){
                listOfStudents.add(new Student(rs.getInt("id"),rs.getString("name"),
                        rs.getString("lastname")));
            }
            rs.close();
            s.close();


        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        for ( Student s: listOfStudents){
            System.out.println(">"+s.getId()+" "+s.getName()+" "+s.getLastname());
        }

        return listOfStudents;

    }

    @Override
    public boolean delete(int id) {



        try {
            Connection c = DatabaseConnector.getInstance().getConnection();

            String query=QueryHelper.buidDeleteQuery("student",id);
            Statement s = c.createStatement();
            int countRows =s.executeUpdate(query);
            //wersja bez helpera:

            //String query="DELETE FROM `student` WHERE `id` = ?";
            //PreparedStatement s = c.prepareStatement(query);
            //s.setInt(1,id);
            //int countRows = s.executeUpdate();

            System.out.println("Usunąłem " + countRows+ " rekordów ");
            s.close();
            return true;


        }catch(SQLException e){
            System.out.println(e.getMessage());
        }


        return false;
    }

    @Override
    public boolean update(int id, Map params) {
        Map<String, String> mp = params;

        try {

            Connection c = DatabaseConnector.getInstance().getConnection();
             String query=QueryHelper.buildUpdateQuery("student",mp);
            Statement ps = c.createStatement();
            ps.executeUpdate(query);

//            String query = "UPDATE `student` SET `name`= ? , `lastname`=? WHERE `id`=?";
//
//            PreparedStatement ps = c.prepareStatement(query);
//
//            ps.setString(1, mp.get("name"));
//            ps.setString(2, mp.get("lastname"));
//            ps.setInt(3, id);
           // ps.executeUpdate();
            ps.close();
            System.out.println("Nadpisałem rekord id=" + id);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return false;
    }


        @Override
    public boolean insert(Map params) {
            Map<String, String> mp = params;
        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            String query="INSERT INTO `student` (`name`,`lastname`) VALUES (?, ?)";

            PreparedStatement ps = c.prepareStatement(query);
            ps.setString(1,mp.get("name"));
            ps.setString(2,mp.get("lastname"));
            ps.executeUpdate();
            ps.close();
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
}
