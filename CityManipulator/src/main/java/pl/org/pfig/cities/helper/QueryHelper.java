package pl.org.pfig.cities.helper;

import com.sun.javafx.binding.StringFormatter;

import java.util.Map;

/**
 * Created by RENT on 2017-07-07.
 */
public class QueryHelper {

    public static String buidDeleteQuery(String tableName, int id){

        return "DELETE FROM `"+tableName+"` WHERE `id`="+id;
    }

    public static String buildSelectQuery(String tableName, Map<String, String> props) {
        String q = "SELECT * FROM `" + tableName + "` WHERE ";
        for (Map.Entry<String, String> e : props.entrySet()) {

            q += generateBracketsValues(false, e.getKey()) +" = "+ generateBracketsValues(true, e.getValue());

        }
        return q;
    }

    public static String buildUpdateQuery(String tableName, Map<String, String> props) {

        String q = "UPDATE `" + tableName + "` SET ";
        String subquery = "";
        int id = Integer.valueOf(props.get("id"));
        props.remove("id");

        for(Map.Entry<String, String> e : props.entrySet()) {
            subquery += " `" + e.getKey() + "` = '" + e.getValue() + "',";
        }
        q += subquery.substring(0, subquery.length() - 1);
        q += " WHERE `id` = " + id;
        return q;
    }

    public static String buildInsertQuery(String tableName, Map<String, String> props) {

        String q = "INSERT INTO `" + tableName + "` VALUES ";
        String subquery = "";

        for (Map.Entry<String, String> e : props.entrySet()) {
            // <name, "Michal">
            subquery += " (`" + e.getKey() + e.getValue() + "');";
            //(`nameMichal`');
        }
        q += subquery.substring(0, subquery.length() - 1);
        return q;
    }


    private static String generateBracketsValues(boolean escape, String ...args) {
        String ret ="( ";
        for (String str: args){
            if(escape) ret+="'";
            ret+=str;
            if(escape) ret+="'";
            ret+=", ";

        }
        if (args.length>0) ret=ret.substring(0,ret.length()-2)+" ";
            ret+=" )";
        return ret;
    }



}
