import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Random;

/**
 * Created by Adrian on 2017-06-27.
 */
public class SortTest {

    interface SortMethod {
        void sort(int[] data);
    }

    public void speedTest(SortMethod sortMethod, int[] data) {
        System.out.println("Test of " + data.length + " elements.");
        long start = System.currentTimeMillis();
        sortMethod.sort(data);
        long end = System.currentTimeMillis();
        long time = end - start;
        System.out.println(time/1000 + "," + time%1000 + " sec.");
        Assertions.assertThat(data).isSorted();
    }

    public void speedTest(SortMethod sortMethod, String name) {
        System.out.println("Test method " + name);
        speedTest(sortMethod, generateData(8*10240));
        speedTest(sortMethod, generateData(2*8*10240));
    }

    private int[] generateData(int size) {
        Random random = new Random();
        int[] data = new int[size];
        for (int i = 0 ; i < size ;i++) {
            data[i] = random.nextInt();
        }
        return data;
    }

    public void testSort(SortMethod sortMethod) {
        int[] data = {1, 7, 2, 4, 3, 8 ,9, 6};
        sortMethod.sort(data);
        Assertions.assertThat(data).isSorted();

    }

    @Test
    public void bubbleSort() throws Exception {
        testSort(Sort::bubbleSort);
    }
    @Test
    public void insertSortTest() throws Exception {
        testSort(Sort::insertSort);
    }
    @Test
    public void insertSortSpeedTest() throws Exception {
        speedTest(Sort::insertSort, "insertSort");
    }
    @Test
    public void bubbleSortSpeedTest() throws Exception {
        speedTest(Sort::bubbleSort, "bubbleSort");
    }

    @Test
    public void selectionSortTest() throws Exception {
        testSort(Sort::selectionSort);
    }
    @Test
    public void selectionSortSpeedTest() throws Exception {
        speedTest(Sort::selectionSort, "selectionSort");
    }
    @Test
    public void testMerge() {
        int[] left = {1,4,8};
        int[] right = {2,3,5};
        int[] result = new int[6];
        Sort.merge(left, right, result);
        Assertions.assertThat(result).isSorted();
    }

    @Test
    public void copyTest() {
        int[] data = {1,22,12,31};
        int[] result = Sort.copy(1, 2, data);
        Assertions.assertThat(result).containsExactly(22,12);
    }
    @Test
    public void mergeSortTest() throws Exception {
        testSort(Sort::mergeSort);
    }
    @Test
    public void mergeSortSpeedTest() throws Exception {
        speedTest(Sort::mergeSort, "mergeSort");
    }
}