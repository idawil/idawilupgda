/**
 * Created by RENT on 2017-06-28.
 */
public class Sort {


    public static void bubbleSort(int data[]) {

        for (int i = 0; i < data.length; i++) {
            for (int j = data.length - 1; j > i; j--) {
                int left = data[j - 1];
                int right = data[j];
                if (left > right) {
                    data[j] = left;
                    data[j - 1] = right;

                }
            }
        }

    }

    public static void insertSort(int data[]) {
        int bufor = 0;
        for (int i = 0; i < data.length; i++) {
            bufor = data[i];
            for (int j = 0; j < i; j++) {
                if (data[i] < data[j]) {
                    for (int k = i; k > j; k--) {
                        data[k] = data[k - 1];
                    }
                    data[j] = bufor;
                }
            }
        }

    }

    public static void selectionSort(int data[]) {

        for (int i = 0; i < data.length; i++) {
            int min = data[i];
            int minIndex = i;
            for (int j = i + 1; j < data.length; j++) {
                if (min > data[j]) {
                    min = data[j];
                    minIndex = j;
                }

            }
            data[minIndex] = data[i];
            data[i] = min;
        }

    }


    public static void mergeSort(int[] data) {
        if (data.length > 1) {
            int middle = data.length / 2;
            int[] left = copy(0, middle - 1, data);
            int[] right = copy(middle, data.length - 1, data);
            mergeSort(left);
            mergeSort(right);
            merge(left, right, data);
        }
    }

    public static void merge(int[] left, int[] right, int[] data) {
        int a = 0;
        int b = 0;
        int i = 0;
        while (a < left.length && b < right.length) {
            if (left[a] <= right[b]) {
                data[i] = left[a];
                a++;
            } else {
                data[i] = right[b];
                b++;
            }
            i++;


        }
        while (a < left.length) {
            i = a + b;
            data[i] = left[a];
            a++;
        }
        while (b < right.length) {
            i = a + b;
            data[i] = right[b];
            b++;
        }


    }

    public static int[] copy(int start, int end, int[] data) {
        int j = 0;
        int[] copy = new int[end - start + 1];
        for (int i = start; i <= end; i++) {
            copy[j++] = data[i];
        }
        return copy;
    }


}
