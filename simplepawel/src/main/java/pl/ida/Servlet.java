package pl.ida;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by RENT on 2017-07-21.
 */
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name=request.getParameter("login");
        String pass=request.getParameter("pass");
        List<String> colors= Arrays.asList("black","green","blue");

        HashMap<String,String> users=new HashMap<String, String>();
        users.put("grzes","testowy");
        users.put("Pawel","Testowy123");

        if(users.containsKey(name)){
            if(users.get(name).equals(pass)){
                request.setAttribute("imie",name);
                request.setAttribute("colors",colors);
                request.getRequestDispatcher("about.jsp").forward(request,response);
            }else{
                request.setAttribute("message","złe hasło");
                request.getRequestDispatcher("error.jsp").forward(request,response);
            }
        }else{
            request.setAttribute("message","Zły login i hasło");
            request.getRequestDispatcher("error.jsp").forward(request,response);
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
