package wyjatki;

public class Square {
		
	public static double square (int n){
		if (n < 0) {
			throw new IllegalArgumentException("Niepoprawny argument");
		}
		return Math.sqrt(n);
	}

}
