package wyjatki;

import java.util.Scanner;

public class Division {

	public static double divide(int a, int b) {

		if (b == 0) {
			throw new IllegalArgumentException("Mianownik r�wny 0");
		}
		return a / b;
	};

	public static double divide(double a, double b) {
		if (b == 0) {
			throw new IllegalArgumentException("Mianownik r�wny 0");
		}
		return a / b;
	};

}
