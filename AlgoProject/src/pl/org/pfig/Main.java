package pl.org.pfig;

import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// zadanie 1.
		// Main.printAddresses("Algorytmiczana", 4);
		Main.RandomNumber();

	}

	public static void printAddresses(String street, int num) {
		String cage = "";
		for (int i = 1; i < num; i += 2) {
			for (int j = 1; j < 13; j++) {
				if (j <= 6) {
					cage = "A";
				} else {
					cage = "B";
				}
				System.out.println("ul. " + i + " " + num + "/" + cage + "/" + j);
			}

		}

	}

	public static void RandomNumber() {
		// Losowanie liczby z przedziału <0,100>
		Random r = new Random();
		int rand = r.nextInt(100);
		System.out.println(rand);
		Scanner sc = new Scanner(System.in);
		// Wpisanie liczny przez użytkownika- max 10 razy
		for (int i = 0; i < 10; i++) {
			System.out.println("Podaj liczbę (proba nr: " + (i + 1) + ")");
			int x = sc.nextInt();

			// Jeśli liczba jest rowna wylosowanej wyswietl komunikat i zakończ
			if (x == rand) {
				System.out.println("Liczby sa rowne");
				sc.close();
				break;
			}
			// jeśli nie, użytkownik podaje kolejną nie wychodzimy z petli
		}

	}
}
