package exercises;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by RENT on 2017-06-13.
 */
public class MultiAAndBReturn {
    public static void main(String[] args) {
        List<Integer> l=retAandB();
        System.out.println(l);
    }


    public static List<Integer> retAandB ()

    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbę a:");
        int a = sc.nextInt();
        System.out.println("Podaj liczbę b:");
        int b= sc.nextInt();
        sc.close();

        List<Integer> list= new ArrayList<Integer>();

        if (a < b) {
            for (int i = 1; (a * i) < b; i++) {
                list.add(a * i);
                           }
        }
        return list;

    }
}
