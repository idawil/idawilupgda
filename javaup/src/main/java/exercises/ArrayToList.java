package exercises;

import com.sun.javafx.binding.ListExpressionHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-06-13.
 */
public class ArrayToList {
    public static void main(String[] args) {
        int[] tab={1,3,4};
        List<Integer> list = getList(tab);

        System.out.println(list);

    }

    private static List<Integer> getList(int[] array) {

        List<Integer> list = new ArrayList<Integer>();
        for(int i=0; i<array.length;i++){
            list.add(array[i]);
        }
        return list;
    }
}
