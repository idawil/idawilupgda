package exercises;

/**
 * Created by RENT on 2017-06-13.
 */
public class PrimeNumber {
    public static void main(String[] args) {

        System.out.println(primeNumber(5));

    }


    private static boolean primeNumber(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}

