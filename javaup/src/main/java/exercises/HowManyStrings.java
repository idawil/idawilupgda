package exercises;

import com.sun.org.apache.xpath.internal.SourceTree;

/**
 * Created by RENT on 2017-06-13.
 */
public class HowManyStrings {
    public static void main(String[] args) {


        String t = "Ala ma kota";
        char[] text = t.toLowerCase().toCharArray();

        char letter = 'a';
        int count = 0;
        for (int i = 0; i < text.length; i++) {

            if (letter == text[i]) {
                count++;
            }
        }
        System.out.println("Litera \'"+letter+"\' wystepuje "+count+" razy.");
    }}
