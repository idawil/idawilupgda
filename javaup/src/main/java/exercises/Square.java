package exercises;

/**
 * Created by RENT on 2017-06-13.
 */
public class Square {
    public static void main(String[] args) {
        int[] tab= {1,2,3};
        int[] sq = square(tab);
        for (int i=0;i<tab.length;i++
             ) {
            System.out.println(sq[i]);
        }
    }

    public static int[] square(int[] tab) {
        int[] sq=new int [tab.length];
        for(int i=0;i<tab.length;i++){
            sq[i]=tab[i]*tab[i];
        }
        return sq;
    }
}
