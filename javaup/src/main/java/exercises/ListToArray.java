package exercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by RENT on 2017-06-13.
 */
public class ListToArray {
    public static void main(String[] args) {

        List<Integer> list = Arrays.asList(1,23,4);
        for(int i=0; i<list.size();i++) {
            System.out.println(listToArray(list)[i]);
        }
    }

    private static int[] listToArray(List<Integer> list) {
        int[] tab=new int[list.size()];
        for (int i = 0; i <list.size();i++) {
            tab[i]=list.get(i);
        }
        return tab;
    }
}