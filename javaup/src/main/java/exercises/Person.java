package exercises;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by RENT on 2017-06-13.
 */
public class Person {


    private String name;
    private String surname;

    public Person(){}
    public Person(String name, String surname){
       this.name=name;
       this.surname=surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

public static String getRandomPerson(){
      return randomPerson().getName()+" "+randomPerson().getSurname(); }



    public static Person randomPerson(){
        List<String> names= Arrays.asList("Anna","Tomasz","Jan","Karolina");
        List<String> surnames=Arrays.asList("Nowak","Kowal","Kot","Kruk");
        Random generator = new Random();

        int i = generator.nextInt(names.size());
       String name= names.get(i);
        i = generator.nextInt(names.size());
        String surname=surnames.get(i);

        return new Person(name,surname);
    }


}
