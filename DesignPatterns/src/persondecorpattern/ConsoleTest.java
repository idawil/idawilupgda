package persondecorpattern;

import static persondecorpattern.PrintBuilder.personWho;

/**
 * Created by RENT on 2017-06-23.
 */
public class ConsoleTest {
    public static void main(String[] args) {
        PersonPrinter personPrinter =new EyesColorPrinter(new BasicDataPrinter());

        Person person = new Person("Jan", "Kowalski", 20, 81.5, 175.0, Person.EyeColor.BLUE);



        PersonPrinter p1= personWho().printAge().printEyesColor().printHeight().printWeight().getHim();


        FilePrinter fp=new FilePrinter(p1);
        fp.printToFile(person,"out.txt");



    }



}
