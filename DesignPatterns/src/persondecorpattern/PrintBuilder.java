package persondecorpattern;

/**
 * Created by RENT on 2017-06-23.
 */
public class PrintBuilder {

    private PersonPrinter personPrinter =new BasicDataPrinter();
    public PrintBuilder printAge(){
        personPrinter =new AgePrinter(personPrinter);
        return this;
    }

    public PrintBuilder printEyesColor(){
        personPrinter=new EyesColorPrinter(personPrinter);
        return this;
    }

    public PrintBuilder printHeight(){
        personPrinter=new HeightPrinter(personPrinter);
        return this;
    }

    public PrintBuilder printWeight(){
        personPrinter=new WeightPrinter(personPrinter);
        return this;
    }

    public static PrintBuilder personWho() {
        return new PrintBuilder();
    }

    public PersonPrinter getHim() {
        return personPrinter;
    }
}
