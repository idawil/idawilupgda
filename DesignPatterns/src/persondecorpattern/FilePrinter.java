package persondecorpattern;

import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class FilePrinter {
    private final PersonPrinter pp;
    public FilePrinter(PersonPrinter pp){
        this.pp = pp;
    }
    public void printToFile(Person person, String filename){
        try (PrintStream out = new PrintStream(filename))
        {
            pp.print(person,out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
