package pl.org.pfig;

/**
 * Created by RENT on 2017-06-23.
 */
public interface Sportsman {
    void prepare();
    void doPumps(int number);
    void doSquats(int number);
    void doCrunches(int number);
}
