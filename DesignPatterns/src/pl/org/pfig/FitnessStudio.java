package pl.org.pfig;

/**
 * Created by RENT on 2017-06-23.
 */
public class FitnessStudio {
    public  void train(Sportsman sportsman){
        sportsman.prepare();
        sportsman.doPumps(10);
        sportsman.doSquats(25);
        sportsman.doCrunches(50);

    }
}
