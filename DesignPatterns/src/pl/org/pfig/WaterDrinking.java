package pl.org.pfig;

/**
 * Created by RENT on 2017-06-23.
 */
public class WaterDrinking implements Sportsman {
    private Sportsman sportsman;

    public WaterDrinking(Sportsman sportsman) {
        this.sportsman = sportsman;
    }

    @Override
    public void prepare() {
        sportsman.prepare();
        System.out.println("Piję wodę");

    }

    @Override
    public void doPumps(int number) {
        sportsman.doPumps(number);
    }

    @Override
    public void doSquats(int number) {
        sportsman.doSquats(number);
    }

    @Override
    public void doCrunches(int number) {
        sportsman.doCrunches(number);
    }
}
