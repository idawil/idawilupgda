package pl.org.pfig;

public class Main {

    public static void main(String[] args) {
        Sportsman sportsman = new WaterDrinking(new BasicSportsman());
       // sportsman.prepare();

        FitnessStudio fitnessStudio = new FitnessStudio();
        //fitnessStudio.train(sportsman);

        Sportsman sp = new WaterDrinking(new DoubleSeries(new BasicSportsman()));
        //fitnessStudio.train(sp);

        Sportsman sp2=new SelfieMaking(new WaterDrinking(new BasicSportsman()));
        //fitnessStudio.train(sp2);

        Sportsman sp3=new NoPrepare(new WaterDrinking(new DoubleSeries(new BasicSportsman())));

        fitnessStudio.train(sp3);



    }
}
