package pl.org.pfig;

/**
 * Created by RENT on 2017-06-23.
 */
public class DoubleSeries implements Sportsman {

    private Sportsman sportsman;

    public DoubleSeries(Sportsman sportsman) {
        this.sportsman = sportsman;
    }

    @Override
    public void prepare() {
    sportsman.prepare();
    }

    @Override
    public void doPumps(int number) {
    sportsman.doPumps(number);
    System.out.println("Druga seria:");
    sportsman.doPumps(number);
    }

    @Override
    public void doSquats(int number) {
    sportsman.doSquats(number);
        System.out.println("Druga seria:");
        sportsman.doSquats(number);
    }

    @Override
    public void doCrunches(int number) {
    sportsman.doCrunches(number);
        System.out.println("Druga seria:");
        sportsman.doCrunches(number);
    }
}
