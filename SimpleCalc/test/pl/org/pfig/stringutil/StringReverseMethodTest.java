package pl.org.pfig.stringutil;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StringReverseMethodTest {
StringUtil str;
	
	@Before
	public void init(){
		str=new StringUtil();
	}
	@Test
	public void whenStringDontHasAnySpacing() {
		String s="alamakota";
		String newS="atokamala";
		assertEquals(str.reverse(s),newS);
	}
	@Test
	public void whenStringHasGotSpacing() {
		String s="a lamakota";
		String newS="atokamal a";
		assertEquals(str.reverse(s),newS);
	}
	@Test
	public void whenStringHasGotOnlyOneLetter() {
		String s="a";
		String newS="a";
		assertEquals(str.reverse(s),newS);
	}
	
	@Test
	public void whenStringIsEmpty() {
		String s="";
		String newS="";
		assertEquals(str.reverse(s),newS);
	}
	
	


}
