package pl.org.pfig.stringutil;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StringUtilLetterSpacingMethodTest {
	StringUtil str;
	
	@Before
	public void init(){
		str=new StringUtil();
	}
	@Test
	public void whenStringDoesntHaveAnySpacing() {
		String s="alamakota";
		String newS="a l a m a k o t a ";
		assertEquals(str.letterSpacing(s),newS);
	}
	
	//nie przechodzi:
	@Test
	public void whenStringHasGotSpacing() {
		String s="a lamakota";
		String newS="a l a m a k o t a ";
		assertEquals(str.letterSpacing(s),newS);
	}
	
	

}
