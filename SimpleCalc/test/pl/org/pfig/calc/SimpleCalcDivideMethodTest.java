package pl.org.pfig.calc;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SimpleCalcDivideMethodTest {
	private SimpleCalc sc;

	@Before
	public void doStuff() {
		sc = new SimpleCalc();
	}

	@Test
	public void whenTwoPositiveNumberAreGivenPositiveNumberAreDivide() {
		int a = 3, b = 6;

		assertEquals(0.5, sc.divide(a, b), 0.01);
	}

	@Test
	public void whenTwoNegativeNumberAreGivenPositiveNumberAreDivide() {
		int a = -3, b = -6;

		assertEquals(0.5, sc.divide(a, b), 0.01);
	}

	@Test
	public void whenOnePositiveNumberAndOneNegativeNumberAreGivenNegativeNumberAreDivide() {
		double a = -3.0, b = 6.0;

		assertEquals(-0.5, sc.divide(a, b), 0.01);
	}

	@Test
	public void whenOneNegativeNumberAndOnePositiveNumberAreGivenNegativeNumberAreDivide() {
		double a = -3.0, b = 6.0;

		assertEquals(-0.5, sc.divide(a, b), 0.01);
	}

	@Test(expected=IllegalArgumentException.class)
	public void whenOnePositiveNumberIsDevideByZero() throws Exception {
		double a = 3.0, b = 0.0;
		sc.divide(a, b);
	}

}
