package pl.org.pfig.calc;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SimpleCalcAddMethodTest {
	
	private SimpleCalc sc;
	
	@Before
	public void doStuff(){
		sc=new SimpleCalc();
	}
	
	@Test
	public void whenTwoPositiveNumbersAreGivenPositiveNumberAreSum() {
		int a = 3, b = 6;
	SimpleCalc sc = new SimpleCalc();
		assertEquals(9, sc.add(a, b));

	}
	
	@Test
	public void whenTwoNegativeNumbersAreGivenNegativeNumberAreSum() {
		int a = -3, b = -6;
		
		assertEquals(-9, sc.add(a, b));

	}
	
	@Test
	public void whenOneNegativeNumberAndOnePositiveNumberAreGivenNegativeNumberAreSum() {
		int a = -7, b = 6;
		
		assertEquals(-1, sc.add(a, b));

	}
	
	@Test
	public void whenOneNegativeNumberAndOnePositiveNumberAreGivenPositiveNumberAreSum() {
		int a = -7, b = 8;
		
		assertEquals(1, sc.add(a, b));

	}
	
	@Test
	public void whenOnePositiveNumberAndOneNegativeNumberAreGivenNegativeNumberAreSum() {
		int a = 6, b = -7;
	
		assertEquals(-1, sc.add(a, b));

	}
	
	@Test
	public void whenOnePositiveNumberAndOneNegativeNumberAreGivenPositiveNumberAreSum() {
		int a = 8, b = -7;
		
		assertEquals(1, sc.add(a, b));

	}
	
	
	
	@Test(expected = Exception.class)
	public void whenExThrowMethodIsUsedExceptionIsTrown() throws Exception {
		
		sc.exThrow();
	}

	
	@Test
	public void whenTwoMaxIntegerAreGivenPositiveNumberAreExpected(){
		
		assertTrue("Out of range",sc.add(Integer.MAX_VALUE,Integer.MAX_VALUE)>0);
	}
}
