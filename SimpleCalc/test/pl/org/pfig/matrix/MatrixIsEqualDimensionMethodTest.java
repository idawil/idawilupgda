package pl.org.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixIsEqualDimensionMethodTest {
	Matrix m;
	
	@Before
	public void init(){
		m=new Matrix();
	}

	@Test
	public void whenMatrixsAreDifferent() {
		int[][] a= new int [2][3] ;
		int[][] b= new int[5][4];
		boolean expected=false;
		assertEquals(expected,m.isEqualDimension(a, b));
	}
	@Test
	public void whenMatrixsIseEqualDimension() {
		int[][] a= new int [2][3] ;
		int[][] b= new int[2][3];
		boolean expected=true;
		assertEquals(expected,m.isEqualDimension(a, b));
	}
	
	@Test	
	public void whenMatrixsAreOneDifferentDimension() {
		int[][] a= new int [2][3] ;
		int[][] b= new int[2][4];
		boolean expected=false;
		assertEquals(expected,m.isEqualDimension(a, b));
	}
	
	

}
