package pl.org.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixIndexedMatrixMethodTest {

	Matrix m;
	
	@Before
	public void init(){
		m=new Matrix();
	}
	
	
	@Test
	public void whenEmptyArrayGivenIndexedMatrixExpected() {
		int [][]actual=new int [3][3];
		int[][] expected={{1,2,3},{4,5,6},{7,8,9}};
		actual=m.indexedMatrix(actual);
		
		for(int i=0;i<actual.length;i++){
			for(int j=0; j<actual[i].length;j++){
				assertEquals(expected[i][j],actual[i][j]);
			}
		}
	}

}
