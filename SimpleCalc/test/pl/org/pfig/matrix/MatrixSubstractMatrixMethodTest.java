package pl.org.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixSubstractMatrixMethodTest {

	
Matrix m;
	
	@Before
	public void init(){
		m=new Matrix();
	}
	@Test
	public void whenMatrixsAreEqualeDimensions() {
		int[][] a= {{1,2},{3,4}};
		int[][] b= {{2,3},{5,7}};
		int[][] exp={{-1,-1},{-2,-3}};
		assertArrayEquals(exp,m.substractMatrix(a, b));
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void whenMatrixsAreDifferentDimensions() throws Exception {
		int[][] a= {{1,2},{3,4,5}};
		int[][] b= {{2,3},{5,7}};
		
		m.substractMatrix(a, b);
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void whenOneMatrixIsEmpty() throws Exception{
		int[][] a= {{1,2},{3,4,5}};
		int[][] b= {{},{}};
		
		m.substractMatrix(a, b);
	}

}
