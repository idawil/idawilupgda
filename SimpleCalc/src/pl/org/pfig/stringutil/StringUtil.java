package pl.org.pfig.stringutil;



import java.util.*;

public class StringUtil {

	// Napiszmy klasę StringUtil, która będzie posiadała konstruktor
	// sparametryzowany którego zadaniem będzie przypisanie stringa do pola
	// prywatnego. Następnie dodajmy metody:

	public StringUtil() { }

	public String letterSpacing(String str) {
		// rozdziela string spacjami
		String[] temp = str.split("");
		str = "";
		for (int i = 0; i < temp.length; i++) {
			str += temp[i] + " ";
		}
		return str;
	}

	public String reverse(String str) {
		String temp = "";
		for (int i = str.length() - 1; i >= 0; i--) {
			temp += str.charAt(i);
		}
		return temp;
	}

	public String getAlphabet() {
		// zwraca string z alfabetem 'abcdefgh...z';
		String string = "";
		for (char i = 97; i < 123; i++) {
			string += i;
		}
		return string;
	}

	public String getFirstLetter(String str) {
		// zwraca pierwszą literę
		return "" + str.charAt(0);
	}

	public String limit(String str, int n) {
		// ucina string we wskazanym miejscu
		String temp = "";
		for (int i = 0; i < n; i++) {
			temp += str.charAt(i);
		}
		return temp;
	}

	public String insertAt(String baseString, String insertString, int n) {
		// umieszcza zadany string pod wybrana pozycja
		if (n > baseString.length()) {
			for (int i = 0; i < n - baseString.length(); i++) {
				baseString += " ";
			}
		}
		baseString = baseString.substring(0, n) + insertString + baseString.substring(n);
		return baseString;
	}


	public String swapLetters(String str) {
		// zwraca ciąg znaków z zamienioną pierwszą i ostatnią literą.
		String[] temp = str.split("");
		str = temp[temp.length - 1];
		for (int i = 1; i < temp.length - 1; i++) {
			str += temp[i];
		}
		str += temp[0];
		return str;
	}

	public String createSentence(String str) {
		// zwraca ciąg znaków z pierwszą wielką literą i z kropką na końcu.
		// Należy sprawdzić, czy kropka na końcu nie występowała wcześniej.

		str = str.substring(0, 1).toUpperCase() + str.substring(1);
		if (!(str.endsWith("."))) {
			str += ".";
		}
		return str;
	}

	public String cut(String str, int from, int to) {
		// zwraca obcięty string od..do
		// (bez użycia metody substring z klasy String!)
		String[] temp = str.split("");
		str = "";
		if (to >= temp.length) {
			to = temp.length - 1;
		}
		for (int i = from; i < to; i++) {
			str += temp[i];
		}
		return str;
	}

	public String pokemon(String str) {
		// ZwRaCa cIaG zNaKoW pIsMeM pOkEmOn ;)
		String[] temp = str.split("");
		str = "";
		for (int i = 0; i < temp.length; i++) {
			str += (i % 2 == 0) ? temp[i].toUpperCase() : temp[i].toLowerCase();
		}
		return str;
	}

	public String getRandomHash(int n) {
		// przypisuje do zmiennej n losowych znakow z zakresu [0-9a-f];
		String str = "";
		int[] temp = new int[2];
		Random random = new Random();
		int number;
		int i = 0;
		int k = 0;
		while(i < n) {
			number = random.nextInt(2);
			if (number == 0 && (temp[0] != number || temp[1] != number)) {
				str += random.nextInt(10);
				temp[k] = number;
				k++;
				i++;
			} else if(number == 1 && (temp[0] != number || temp[1] != number)) {
				str += (char) (97 + random.nextInt(7));
				temp[k] = number;
				i++;
				k++;
			}
			if(k==2) {
				k = 0;
			}
		}
		return str;
	}

	// Oraz metody dodatkowe, nie operujące na wczytanym stringu:

	public String join(String[] arrayOfStrings, String delimeter) {
		// zwraca String rozdzielony delimeterem.
		String result = arrayOfStrings[0];
		for(int i = 1; i < arrayOfStrings.length; i++) {
			result += delimeter + arrayOfStrings[i];
		}
		return result;
	}

}