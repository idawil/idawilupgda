package pl.ida.repository;

import org.springframework.data.repository.CrudRepository;
import pl.ida.entity.Staff;

/**
 * Created by RENT on 2017-07-27.
 */
public interface StaffRepository extends CrudRepository<Staff, Long>{
}
