package pl.ida.repository;

import org.springframework.data.repository.CrudRepository;
import pl.ida.entity.Animal;

/**
 * Created by RENT on 2017-07-26.
 */
public interface AnimalRepository extends CrudRepository<Animal, Long> {
}
