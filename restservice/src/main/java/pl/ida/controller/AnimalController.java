package pl.ida.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.ida.entity.Animal;
import pl.ida.entity.Specie;
import pl.ida.repository.AnimalRepository;
import pl.ida.repository.SpecieRepository;

import java.util.*;



@CrossOrigin
@RestController
@RequestMapping("/animals")
public class AnimalController {

    @Autowired
    private AnimalRepository animalRepository;
    @Autowired
    private SpecieRepository specieRepository;


    @RequestMapping("/")
    public String animal() {
        return "";
    }

    @RequestMapping("/show")
    public List<Animal> listAnimals() {
        return (List<Animal>) animalRepository.findAll();
    }


    @RequestMapping("/add")
    public Animal addAnimal(@RequestParam(name = "name") String name,
                            @RequestParam(name = "description") String desc,
                            @RequestParam(name = "image") String image,
                            @RequestParam(name="specie") String specie) {
        long specieId=Long.valueOf(specie);
        Specie s=specieRepository.findOne(specieId);

        Animal myAnimal=new Animal();
        myAnimal.setName(name);
        myAnimal.setDescription(desc);
        myAnimal.setImage(image);

        myAnimal.setSpecie(s);
        return animalRepository.save(myAnimal);
    }

    @RequestMapping("/show/{id}")
    public Animal showAnimalByID(@PathVariable("id") String id) {
        long myId = Long.valueOf(id);

        return animalRepository.findOne(myId);
    }



}