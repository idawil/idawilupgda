package pl.ida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.ida.entity.Profession;
import pl.ida.entity.Staff;
import pl.ida.repository.ProfessionRepository;
import pl.ida.repository.StaffRepository;

import java.util.List;

/**
 * Created by RENT on 2017-07-27.
 */
@CrossOrigin
@RestController
@RequestMapping("/staff2")
public class StaffController {
    @Autowired
    private StaffRepository staffRepository;
    @Autowired
    private ProfessionRepository professionRepository;

    @RequestMapping("/show")
    public List<Staff> listOfStaff() {
        return (List<Staff>) staffRepository.findAll();
    }

    @RequestMapping("/add")
    public Staff addStaff(@RequestParam(name="name")String name,
                            @RequestParam(name = "lastname") String lastname,
                                    @RequestParam(name = "salary") double salary,
                                            @RequestParam (name="profession") String profession){
        long professionId=Long.valueOf(profession);
        Profession p=professionRepository.findOne(professionId);
        Staff s=new Staff();
        s.setName(name);
        s.setLastname(lastname);
        s.setSalary(salary);
        s.setProfession(p);
        return staffRepository.save(s);

        }
    @RequestMapping("/show/{id}")
    public Staff showStaffByID(@PathVariable("id") String id){
        long myId=Long.valueOf(id);
        return staffRepository.findOne(myId);
    }


}
