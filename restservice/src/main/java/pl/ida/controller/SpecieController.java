package pl.ida.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.ida.entity.Specie;
import pl.ida.repository.SpecieRepository;
import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/species")
public class SpecieController {

    @Autowired
    private SpecieRepository specieRepository;



    @RequestMapping("/add")
    public Specie add(@RequestParam(name="name") String name,
                      @RequestParam(name="description") String description){
        Specie mySpecie=new Specie();
        mySpecie.setName(name);
        mySpecie.setDescription(description);
        return specieRepository.save(mySpecie);
    }

    @RequestMapping("/show")
    public List<Specie> showAll() {
        return (List<Specie>) specieRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Specie showById(@PathVariable(name = "id") String id){
        long myId = Long.valueOf(id);
        return specieRepository.findOne(myId);
    }

}
