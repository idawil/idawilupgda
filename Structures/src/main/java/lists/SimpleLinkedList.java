package lists;

import java.util.NoSuchElementException;


public class SimpleLinkedList implements SimpleList {

    private Element first;
    private Element last;
    private int number=0;

    @Override
    public void add(int value) {

        Element element = new Element();
        element.value = value;
        if (first == null) {
            last = element;
            first = element;
        } else {
            last.next = element;
            element.prev=last;
            last = last.next;
        }
        number++;

    }

    @Override
    public int get(int index) {
        if (index < number) {
            Element element = first;
            for (int i = 0; i < index; i++) {
                element = element.next;
            }
            return element.value;
        } else {
            throw new NoSuchElementException();
        }

    }

    @Override
    public void add(int value, int index) {
        Element element = new Element();
        element.value = value;
        if (index == 0) {
            element.next = first;
            first = element;
            if (last==null){
                last = element;
            }
        } else if (index == number) {
            add(value);
        } else if (index < number) {
            Element right = first;
            for (int i = 0; i < index; i++) {
                right = right.next;
            }
            Element left = right.prev;
            element.prev = left;
            element.next = right;
            left.next = element;
            right.prev = element;

        } else {
            throw new NoSuchElementException();
        }
        number++;
    }

    @Override
    public boolean contain(int value) {
        Element element = first;
        while (element!=null) {
            if (element.value == value) {
                return true;
            }
            element = element.next;
        }

        return false;
    }

    @Override
    public void remove(int index) {

        if(index==0){
            first.next=first;
            first.prev=null;
        }else if (index < number) {
            Element element = first;

            for (int i = 0; i < index; i++) {
                element = element.next;
            }
            remove(element);
        }
        else throw new NoSuchElementException();


    }

    private void remove(Element element) {

        if(element.next==null&&element.prev==null){
            last=null;
            first=null;
        }
        else if (element.next==null){
            last.prev=last;
            last.next=null;
        }else if(element.prev==null){
            first=first.next;
            first.prev=null;
        }
        else{
        Element left = element.prev;
        Element right = element.next;
        left.next = right;
        right.prev = left;}
        number--;
    }

    @Override
    public void removeValue(int value) {
        Element element = first;

        for (int i = 0; i < number; i++) {
            if(element.value==value){

                remove(element);
            }
            element = element.next;
                    }
                    }

@Override
public int size() {
        return number;
        }

private static class Element {
    int value;
    Element next;
    Element prev;
}
}