package FIFO;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleLinkedQueue implements SimpleQueue {

    private Element first;
    private Element last;


    @Override
    public boolean isEmpty() {
        if(first==null){
            return true;
        }
        return false;
    }



    @Override
    public void offer(int value) {

        Element element=new Element(value);
        if(isEmpty()){
            first=element;
            last=element;
            }else{
            last.next=element;
            last=element;
            }

    }



    @Override
    public int poll() {

        if (!isEmpty()){
            int result=first.value;
            first=first.next;
            if (first == null) {
                last = null;
            }
            return result;
        }else{
            throw new NoSuchElementException("Empty List");
        }


    }

    @Override
    public int peek() {
        if (isEmpty())throw new NoSuchElementException("Empty List");
        return first.value;
    }

    private static class Element {
        int value;
        Element next;
        public Element(int value) {
            this.value = value;
        }

    }
}
