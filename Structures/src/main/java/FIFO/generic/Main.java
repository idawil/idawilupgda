package FIFO.generic;

/**
 * Created by RENT on 2017-06-27.
 */
public class Main {

    public static void main(String[] args) {
        SimpleQueue<String> simpleQueue=new SimpleArrayQueue<>();
        simpleQueue.offer("Ala");
        simpleQueue.offer("Ola");
        simpleQueue.offer("Ania");
        System.out.println(simpleQueue.peek());
        System.out.println(simpleQueue.poll());
        System.out.println(simpleQueue.poll());
        System.out.println(simpleQueue.isEmpty());
        System.out.println(simpleQueue.poll());
        System.out.println(simpleQueue.isEmpty());


        SimpleQueue<String> simpleQueue1=new SimpleLinkedQueue<>();
        simpleQueue1.offer("Ala");
        simpleQueue1.offer("Ola");
        simpleQueue1.offer("Ania");
        System.out.println(simpleQueue1.peek());
        System.out.println(simpleQueue1.poll());
        System.out.println(simpleQueue1.poll());
        System.out.println(simpleQueue1.isEmpty());
        System.out.println(simpleQueue1.poll());
        System.out.println(simpleQueue1.isEmpty());

    }
}
