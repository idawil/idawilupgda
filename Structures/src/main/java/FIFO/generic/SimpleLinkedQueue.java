package FIFO.generic;


import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleLinkedQueue<T> implements SimpleQueue<T> {

    private Element<T> first;
    private Element<T> last;


    @Override
    public boolean isEmpty() {
        return first == null;
    }


    @Override
    public void offer(T value) {

        Element<T> element = new Element<>(value);
        element.value = value;
        if (isEmpty()) {
            first = element;
            last = element;
        } else {
            last.next = element;
            last = element;
        }

    }


    @Override
    public T poll() {

        if (!isEmpty()) {
            T result = first.value;
            first = first.next;
            if (first == null) {
                last = null;
            }
            return result;
        } else {
            throw new NoSuchElementException("Empty List");
        }


    }

    @Override
    public T peek() {
        if (isEmpty()) throw new NoSuchElementException("Empty List");
        return first.value;
    }

    private static class Element<T> {
        T value;
        Element<T> next;

        public Element(T value) {
            this.value = value;
        }

    }
}
