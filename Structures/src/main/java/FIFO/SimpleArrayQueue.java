package FIFO;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleArrayQueue implements SimpleQueue {


    private int [] data =new int[8];
    private int start=0;
    private int end=0;
    private boolean isEmpty=true;

    @Override
    public boolean isEmpty() {

        return isEmpty;
    }

    @Override
    public void offer(int value) {
        if(isFull()) {
            doubleLength();
        }
            data[end] = value;
            end = (end + 1) % data.length;
            isEmpty = false;

    }

    private boolean isFull() {
        return (start==end)&&(!isEmpty);
    }

    private void doubleLength() {
        int[]array=new int[2*data.length];
        int j=0;
        while (!isEmpty())    {
            array[j]=poll();
            j++;
        }
        data=array;
        start=0;
        end=j;
    }

    @Override
    public int poll() {
        int ret=start;
        if(isEmpty){
            throw new RuntimeException("EmptyQueu");
        }else{

        start=(start+1)%data.length;}

        if (start==end){isEmpty=true;}
        return data[ret];
    }

    @Override
    public int peek() {
        if(isEmpty){
            throw new RuntimeException("EmptyQueu");
        }
        return data[start];
    }
}
