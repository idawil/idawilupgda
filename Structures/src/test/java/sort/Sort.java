package sort;

/**
 * Created by RENT on 2017-06-27.
 */
public class Sort {
    public static void main(String[] args) {


        int[] data = {100, 11, 5, 50, 0};
        int index = 0;
        for (int i = 0; i < data.length; i++) {
            int a = data[i];

            for (int j = i + 1; j < data.length; j++) {
                a = Math.min(a, data[j]);
                index = j;
            }
            if (a != data[i]) {
                data[index] = data[i];
                data[i] = a;
            }
        }

        for (int i = 0; i < data.length; i++) {
            System.out.println(data[i]);
        }
    }

}
