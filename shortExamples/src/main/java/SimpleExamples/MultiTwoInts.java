package SimpleExamples;

public class MultiTwoInts extends TwoInts {
	private int amount;
	
	public MultiTwoInts(int a, int b, int amount){
		super(a,b);
		this.amount=amount;
	}
	
	
	public int add(){
		return super.add()*amount;
	}
}
