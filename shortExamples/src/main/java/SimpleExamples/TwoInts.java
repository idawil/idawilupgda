package SimpleExamples;

public class TwoInts {
	private int a;
	private int b;
	
	
	
	public TwoInts(int a, int b) {
		this.a = a;
		this.b = b;
	}


	public int add(){
		return this.a+this.b;
	}
	

	public static void main(String[] args) {
		TwoInts twoInts=new TwoInts(10,5);
		System.out.println(twoInts.add());
	}
}
