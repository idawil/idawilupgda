package SimpleExamples;

public class TwoIntsMath {
	public static int add(int a, int b){
		return a+b;
	}
	
	
	public static int add(int a, int b,int amount){
		return (a+b)*amount;
	}
	
	public static void main(String[] args) {
		System.out.println(TwoIntsMath.add(4, 20));
		System.out.println(TwoIntsMath.add(4, 20,2));
	}
}
