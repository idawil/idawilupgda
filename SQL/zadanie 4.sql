DROP DATABASE IF EXISTS shop;
SET SQL_SAFE_UPDATES=0;
CREATE DATABASE shop;
USE shop;



CREATE TABLE category (
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20),
description TEXT
);

CREATE TABLE product (
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20),
price DECIMAL(6,2),
category_id INT,
FOREIGN KEY(category_id) REFERENCES category(id)
);

INSERT INTO category (name, description) VALUES
('pieczywo','bułki, chleb'),
('chemia','środki czystości konsmetyki'),
('warzywa','warzywa świeże');

SELECT * FROM category;

INSERT INTO product (name,price,category_id) VALUES
('marchew',2,3),
('mydło',2,2),
('płyn do prania',14,2),
('ziemniaki',1,3),
('chleb razowy',3,1),
('bułka kajzerka',1,1),
('ogórek',4,3),
('bagietka',1,1),
('płyn do szyb', 8,2),
('pomidor',3,3);

SELECT p.name, c.name 
FROM product AS p INNER JOIN category AS c 
ON p.category_id=c.id;

SELECT c.name, count(*),avg(price) 
FROM  product AS p  INNER JOIN category AS c  
ON p.category_id=c.id 
GROUP BY p.category_id;

CREATE VIEW product_full AS
SELECT p.id, p.name AS 'product_name', p.price, c.name AS 'category_name' 
FROM  product AS p LEFT JOIN category AS c  
ON p.category_id=c.id
ORDER BY p.id;

SELECT * FROM product_full;

