DROP DATABASE IF EXISTS company;
SET SQL_SAFE_UPDATES=0;
CREATE DATABASE company;
USE company;

CREATE TABLE worker (
ID INT  AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20), 
salary DECIMAL(8,2),
age INT
);

INSERT INTO worker (salary,age) VALUES
(1000,20), (1000,34), (1000,30), (1500,20), (1500,20), 
(1500,31), (2000,50),(2000,34),(4000,34),(4000,20);

SELECT * FROM worker;

SELECT * FROM worker ORDER BY salary;
SELECT * FROM worker ORDER BY salary DESC;


SELECT MIN(salary) AS 'MIN', MAX(salary) AS 'MAX', AVG(salary) AS 'AVG'  FROM worker;

SELECT COUNT(*) AS 'worker_count', salary FROM worker GROUP BY salary;

SELECT AVG(salary) AS 'avg_salary', age FROM worker GROUP BY age ;

SELECT * FROM worker WHERE salary = (SELECT MAX(salary) FROM worker);
SELECT * FROM worker WHERE salary = (SELECT MIN(salary) FROM worker);

SELECT * FROM worker ORDER BY salary LIMIT 1;
SELECT * FROM worker ORDER BY salary DESC LIMIT 1;

UPDATE worker SET name='Janusz' WHERE name IS NULL;

SELECT * FROM worker;

SELECT abs(salary-(SELECT avg(SALARY) FROM worker))as diff,name,age FROM worker ORDER BY diff;

