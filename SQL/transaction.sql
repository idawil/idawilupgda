DROP DATABASE IF EXISTS bank;
SET SQL_SAFE_UPDATES=0;
CREATE DATABASE bank;
USE bank;

CREATE TABLE `account`(
id INT AUTO_INCREMENT PRIMARY KEY,
number CHAR(32),
balance DECIMAL(65,2));

INSERT INTO `account` VALUES
(id,'123', 12900),
(id,'124', 200),
(id,'125', 500);

SELECT * FROM `account`;

START TRANSACTION;
UPDATE `account` SET balance=balance-100 WHERE id=1;
UPDATE `account` SET balance=balance+100 WHERE id=2;
SELECT * FROM `account`;
COMMIT;


START TRANSACTION;
UPDATE `account` SET balance=balance-400 WHERE id=2;
UPDATE `account` SET balance=balance+400 WHERE id=1;
SELECT * FROM `account`;
ROLLBACK;

SELECT * FROM `account`;

