/*Utwórz bazę school z tabelą lesson (subject, teacher_name, teacher_surname, start_time, end_time).
Podziel tabelę na 3: subject, lesson, teacher, lesson_number.
Dodaj 2 nauczycieli, 5 godzin lekcyjnych, 3 przedmioty oraz 4 lekcję do bazy.
Pobierz wszyskie lekcje w formacie: subject, teacher_name, teacher_surname, start_time, end_time.
Utwórz widok lesson_full (subject, teacher_name, teacher_surname, start_time, end_time).
Pobierz wszyskie lekcje z konretnego przedmiotu w formacie: teacher_name, teacher_surname, start_time, end_time.
Pobierz pary (lessons_count, subject).
Dodaj pole day_of_week (MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY);

Pobierz wszystkie lekcje pomiędzy 10:00, a 12:45

*/

DROP DATABASE IF EXISTS school;
SET SQL_SAFE_UPDATES=0;
CREATE DATABASE school;
USE school;

/*CREATE TABLE lesson(
`subject` VARCHAR(20),
teacher_name VARCHAR(20), 
teacher_surname VARCHAR(20),
start_time TIME,
end_time TIME
);*/

CREATE TABLE `subject`(
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20)
);

CREATE TABLE teacher (
id INT AUTO_INCREMENT PRIMARY KEY,
teacher_name VARCHAR(20), 
teacher_surname VARCHAR(20)
);

CREATE TABLE lesson_number (
id INT AUTO_INCREMENT PRIMARY KEY,
start_time TIME,
end_time TIME
);

CREATE TABLE lesson (
PRIMARY KEY (lesson_number_id,teacher_id),
subject_id INT,
lesson_number_id INT,
teacher_id INT,
FOREIGN KEY(subject_id) REFERENCES `subject`(id),
FOREIGN KEY(lesson_number_id) REFERENCES lesson_number(id),
FOREIGN KEY(teacher_id) REFERENCES teacher(id)
);

INSERT INTO teacher VALUES
(id,'Jan','Kowalski'),
(id,'Tomasz','Nowak');

INSERT INTO lesson_number VALUES
(id,'08:00','08:45'),
(id,'08:55','09:40'),
(id,'09:50','10:35'),
(id,'10:45','11:30'),
(id,'12:00','12:45');

INSERT INTO `subject` VALUES
(id, 'Maths'),
(id,'English'),
(id,'Biology');

INSERT INTO lesson (subject_id,
lesson_number_id,
teacher_id) VALUES
(1,1,1),
(2,5,1),
(3,3,1),
(3,4,2);

CREATE VIEW lesson_full AS
SELECT s.name, teacher_name, teacher_surname, start_time, end_time FROM lesson l 
LEFT JOIN `subject` s ON s.id=l.subject_id
LEFT JOIN teacher t ON t.id=l.teacher_id
LEFT JOIN lesson_number ln ON ln.id=l.lesson_number_id;

SELECT * FROM lesson_full;

SELECT teacher_name, teacher_surname, start_time, end_time FROM lesson_full WHERE name='Biology';

SELECT COUNT(*) AS lesson_count, name FROM lesson_full GROUP BY name;

SELECT * FROM lesson_full WHERE  start_time>'09:59:59'  AND end_time<'12:46:00';

/*ALTER TABLE lesson ADD day_of_week ENUM('MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY');

ALTER TABLE lesson DROP PRIMARY KEY, ADD  PRIMARY KEY (lesson_number_id,teacher_id, day_of_week);

UPDATE lesson SET day_of_week='MONDAY' WHERE day_of_week IS NULL;
UPDATE lesson SET day_of_week='WEDNESDAY' WHERE teacher_id=1;

CREATE VIEW lesson_full1 AS
SELECT s.name, teacher_name, teacher_surname, start_time, end_time, day_of_week FROM lesson l 
LEFT JOIN `subject` s ON s.id=l.subject_id
LEFT JOIN teacher t ON t.id=l.teacher_id
LEFT JOIN lesson_number ln ON ln.id=l.lesson_number_id;

SELECT * FROM lesson_full1; */





