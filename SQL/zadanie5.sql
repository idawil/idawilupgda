DROP DATABASE IF EXISTS capitals;
SET SQL_SAFE_UPDATES=0;
CREATE DATABASE capitals;
USE capitals;

CREATE TABLE city (
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20),
citizens INT);

CREATE TABLE state (
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20),
population INT,
capital_id INT,
FOREIGN KEY(capital_id) REFERENCES city(id));

INSERT INTO city (name,citizens) VALUES
('Praga',1280500),
('Paryż',2000000),
('Wilno',539900),
('Berlin',3400000),
('Warszawa',1753977),
('Olsztyn',173500),
('Gdańsk',463700),
('Sopot',37650),
('Gdynia',247500),
('Kraków',762500);

INSERT INTO state (name, population,capital_id) VALUES
('Czechy',10627448,1),
('Francja', 66259012,2),
('Litwa',3505738,3),
('Niemcy',80996685,4),
('Polska',38346279,5);

SELECT s.name, c.name 
FROM state AS s INNER JOIN city AS c
ON s.capital_id=c.id;

SELECT c.name FROM city AS c RIGHT JOIN state AS s ON s.capital_id=c.id;

SELECT s.name AS 'state_name', c.citizens AS 'capital_citizens' 
FROM state AS s INNER JOIN city AS c ON s.capital_id=c.id;

SELECT s.name AS 'state_name' 
FROM state AS s INNER JOIN city AS c ON s.capital_id=c.id
WHERE c.citizens > (s.population*0.1);


ALTER TABLE city ADD is_capital BOOLEAN;


UPDATE city SET is_capital=0;
SELECT * FROM city;


UPDATE city SET is_capital=1  WHERE EXISTS(
SELECT * FROM state WHERE state.capital_id=city.id
) ;

DELETE FROM city WHERE is_capital=0;

SELECT * FROM city;



  




