DROP DATABASE IF EXISTS personal_data;
SET SQL_SAFE_UPDATES=0;
CREATE DATABASE personal_data;

USE personal_data;

CREATE TABLE person(
first_name VARCHAR(20),
last_name VARCHAR(20),
age INT);

INSERT INTO person VALUES("Jan","Nowak",40),
("Ewa","Nowak",20),
("Tomasz","Nowacki",100),
("Jan2","Testowy",33),
("Jan3","Testowy",33);

SELECT first_name FROM person;

SELECT COUNT(*) FROM person;

SELECT COUNT(*), age FROM person GROUP BY age;

UPDATE person SET age=age+1;

SELECT * FROM person;


