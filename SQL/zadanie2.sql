DROP DATABASE IF EXISTS contracts;
CREATE DATABASE contracts;
USE contracts;


CREATE TABLE contract(
ID INT AUTO_INCREMENT PRIMARY KEY, 
first_name VARCHAR(20), 
last_name VARCHAR (20), 
phone_number VARCHAR(9), 
e_mail VARCHAR(30)
);

INSERT INTO contract (first_name, last_name, phone_number, e_mail) VALUES
('Anna','Kowalska','600123123','a.kowalska@gmail.com'),
('Jan','Kowalski','672123122','j.kowalskI@gmail.com'),
('Anita','Kowalska','630123123','a.kowalska@wp.pl'),
('Jan','Nowak','672123122','jnowakI@gmail.com'),
('Anna','Testowa','500123000','a.testowa@wp.pl');


SELECT * FROM contract;
SELECT last_name, phone_number FROM contract;

SELECT CONCAT(first_name," ",last_name) AS 'name', e_mail FROM contract;

SELECT COUNT(*) FROM contract;

INSERT INTO contract (first_name, last_name, phone_number) VALUES ('Janina','Nowak','700123122'),
('Anita','Bezmaila','500123000');

UPDATE contract SET e_mail='brak' WHERE e_mail IS NULL;

SELECT * FROM contract;

SELECT * FROM contract WHERE last_name = 'Nowak';

SELECT * FROM contract WHERE phone_number LIKE '672%';

SELECT * FROM contract WHERE phone_number LIKE '%0';




