SET SQL_SAFE_UPDATES=0;
USE shop;

CREATE TABLE customer (
id INT AUTO_INCREMENT PRIMARY KEY,
first_name VARCHAR(20),
last_name VARCHAR (20)
);



CREATE TABLE `order` (
id INT AUTO_INCREMENT PRIMARY KEY, 
date DATE, 
paid ENUM ('NEW', 'PAID', 'CANCELED', 'COMPLETION'), 
customer_id INT,
FOREIGN KEY(customer_id) REFERENCES customer(id)
);



CREATE TABLE order_products (
PRIMARY KEY (product_id,order_id),
order_id INT , 
product_id INT,
quantity INT UNSIGNED NOT NULL,
FOREIGN KEY(product_id) REFERENCES product(id),
FOREIGN KEY(order_id) REFERENCES  `order`(id)

);

INSERT INTO customer(first_name,last_name) VALUES 
('Jan','Kowalski');

INSERT INTO `order`(date,paid,customer_id) VALUES
('2017-06-03', 'PAID', 1),
('2017-06-01', 'PAID', 1);

INSERT INTO order_products (order_id,product_id, quantity)VALUES
(1,1,3),
(1,2,3),
(1,5,1),
(1,6,1),
(1,3,2),
(2,1,3),
(2,2,3),
(2,7,1),
(2,6,5),
(2,9,2);


SELECT p.name, op.quantity, p.price FROM order_products op INNER JOIN product p 
ON op.product_id=p.id  WHERE op.order_id=1;

CREATE VIEW `super` AS
SELECT o.id, sum(op.quantity) AS 'products_quantity', 
concat(c.first_name," ",c.last_name) AS customer_name, o.date,o.paid,SUM(p.price*op.quantity) AS 'amount'
FROM order_products op 
LEFT JOIN product p  ON op.product_id=p.id 
LEFT JOIN `order` o ON o.id=op.order_id
LEFT JOIN customer c ON o.customer_id=c.id GROUP BY o.id;

SELECT * FROM `super` WHERE date='2017-06-03';


 
