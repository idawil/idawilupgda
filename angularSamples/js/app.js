var app = angular.module('firstApp', []);
app.controller('firstController', ['$scope', '$filter',function($scope,$filter) {
    $scope.message = "Wiadomość z kontrolera #1";
    $scope.user = {
        name: "paweł",
        lastname: $filter('limitTo')("testowy",3),
        salary: 1234.89,
        birthday: 19900707
    };
}]);
app.controller('secondController', function(){
    this.info = "Wiadomość z kontrolera #2";
});
app.controller('thirdController', ['$scope', '$log','limitToFilter', function($scope,$log,limitToFilter){
    $scope.message = limitToFilter("Wiadomość z kontrolera #3",10);
    
    $scope.myList = [
        'pierwsza',
        'druga',
        'trzecia',
        'czwarta',
        'piąta'
        ];
    $scope.showMessege=function(){
        alert($scope.message+' '+$scope.$parent.messege);
    }
    
}]);
app.controller('fourthC',['$scope','$http', function($scope,$http){
    $scope.showCurrencies=function(){
        $http({
            url:'http://api.fixer.io/latest',
            method:'GET',
            dataType:'json',
            params:{}
        }).then(function(res){
            $scope.rates="";
            var rates=res.data.rates;
            var keys=Object.keys(rates);
            var ul=document.createElement('ul');
            for(let key of keys){
                var li=document.createElement('li');
                li.innerHTML=key+': '+rates[key];
                ul.appendChild(li);
            }
            document.getElementById('result').appendChild(ul);
//            console.log(res.data.rates);
        },function(err){
            console.warn(err);
        });
        console.log('czesc jestem pawel');
    }
}])

app.controller('fiveC',['$scope','$http', function($scope,$http){
    $scope.showPeople=function(){
        $http({
            url:'https://randomuser.me/api/',
            method:'GET',
            dataType:'json',
            params:{}
        }).then(function(res){
            var name=res.data.results[0].name;
            var location=res.data.results[0].location; 
            $scope.rates="";
            var rates=res.data.rates;
//            var keys=Object.keys(rates);
           var ul=document.createElement('ul');
//            for(let key of keys){
//                var li=document.createElement('li');
//                li.innerHTML=key;
//                ul.appendChild(li);
//            }
//            document.getElementById('myPeople').appendChild(ul);
          
            
            
           // console.log(res);
            console.log(name.first+' '+name.last+' '+location.city+' '+location.postcode+' '+location.state+' '+location.street);
        
        },function(err){
            console.warn(err);
        });
        console.log('czesc jestem ida');
    }
}])