var app=angular.module('temperature',[]);
app.controller('firstController',[
    '$scope',function($scope){
        
         $scope.computeC= function() {
             $scope.kelvin=$scope.celcius*1+273.15;
             $scope.fahrenheit=$scope.celcius*1.8+32;
             }
         $scope.computeK= function() {
             $scope.celcius=$scope.kelvin*1-273.15;
             $scope.fahrenheit=$scope.kelvin*1.8-459.67;
             }
         $scope.computeF= function() {
             $scope.celcius=($scope.fahrenheit-32)/1.8;
             $scope.kelvin=($scope.fahrenheit*1+459.67)*5/9;
             }
         
    }
]);

