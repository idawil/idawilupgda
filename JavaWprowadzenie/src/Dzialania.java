
public class Dzialania {

	public static void main(String[] args) {
		System.out.println("2+3");
		int a=2+3;
		System.out.println(a);
		System.out.println(2+3);
		
		System.out.println("2-4");
		int b=2-4;
		System.out.println(b);
		System.out.println(2-4);
		
		System.out.println("5/2");
		int c=5/2;
		System.out.println(c);
		System.out.println(5/2);
		
		System.out.println("5.0/2");
		double d= 5.0/2;
		System.out.println(d);
		System.out.println(5.0/2);
		
		System.out.println("5/2.0");
		double e= 5/2.0;
		System.out.println(d);
		System.out.println(5/2.0);
		
		System.out.println("5.0/2.0");
		double f= 5.0/2.0;
		System.out.println(f);
		System.out.println(5.0/2.0);
		
		System.out.println("100L-10");
		long g= 100L-10;
		System.out.println(g);
		System.out.println(100L-10);
		
		System.out.println("2f-3");
		float h= 2f-3;
		System.out.println(h);
		System.out.println(2f-3);
		
		System.out.println("5f/2");
		float i= 5f/2;
		System.out.println(i);
		System.out.println(5f/2);
		
		System.out.println("5d/2");
		double j= 5d/2;
		System.out.println(j);
		System.out.println(5d/2);
		
		System.out.println("'A'+2");
		int k='A'+ 2;
		System.out.println(k);	
		System.out.println('A'+2);
		
		System.out.println("'a'+2");
		int l='a'+2;
		System.out.println(l);
		System.out.println('a'+2);
		
		System.out.println("\"a\"+2");
		String m="a"+2;
		System.out.println(m);
		System.out.println("a"+2);
		
		System.out.println("\"a\"+\"b\"");
		String n="a"+"b";
		System.out.println(n);
		System.out.println("a"+"b");
		
		System.out.println("'a'+'b'");
		int o='a'+'b';
		System.out.println(o);
		System.out.println('a'+'b');
		
		System.out.println("\"b\"+'a'");
		String p="b"+'a';
		System.out.println(p);
		System.out.println("b"+'a');
		
		System.out.println("\"b\"+'a'+3");
		String r="b"+'a'+3;
		System.out.println(r);
		System.out.println("b"+'a'+3);
		
		System.out.println("'b'+3+\"a\"");		
		String s='b'+3+"a";
		System.out.println(s);
		System.out.println('b'+3+"a");
	
		System.out.println("9%4");
		int t=9%4;
		System.out.println(t);
		System.out.println(9%4);
		
		int u=9/4;
		double v=(double)9/4;
		double w=v-u;
		int wynik= (int)(w*4);
		System.out.println(wynik);
				
	
	}

}
