import java.util.Scanner;

public class forwhile {

	
	public static void main(String[] args) {
		Scanner cs = new Scanner(System.in);
		// dividers100();
		// from100to130();
		// litery();
		HelloWorldUser(cs);
		getNegativeNumberFromUser(cs);
		cs.close();
	}

	private static void getNegativeNumberFromUser(Scanner cs) {
		int negative;

		do {
			System.out.println("Podaj liczb� ujemn�");
			negative = cs.nextInt();
		} while (negative >= 0);
	}

	private static void HelloWorldUser(Scanner cs) {
		System.out.println("Podaj liczb�");

		int a = cs.nextInt();

		int i = 1;
		while ((i <= a) && (a != 0)) {
			System.out.println("Hello World!");
			i++;
		}
	}

	private static void litery() {
		for (char i = 'a'; i <= 'z'; i++) {
			System.out.println((char) i);
		}
	}

	private static void dividers100() {
		int i = 1;
		while (i <= 100) {
			if (100 % i == 0) {
				System.out.println(i);
			}
			i++;
		}
	}

	private static void from100to130() {
		int i = 100;
		while (i <= 130) {
			System.out.println(i);
			i = i + 3;
		}
		for (int j = 102; j <= 130; j = j + 3) {
			System.out.println(j);
		}
	}

}