
public class Metody {

	public static void main(String[] args) {

		// suma();
		// iloczyn();
		 //silnia();
		// SumaCiaguAryt();
		// SumaCiaguGeometr();
		// ZliczanieZnakow();

		System.out.println("Suma ciagu arytmetycznego: " + obliczSumeCiagu(5, 3, 1));

		char znak = 'a';
		String text = "Ala ma kota";
		System.out.println(
				"Znak " + znak + " wystepuje w \"" + text + "\" " + MetodaZliczanieZnakow(text, znak) + " razy.");
		
	}

	private static int obliczSumeCiagu(int r, int n, int a0) {
		int element = a0;
		int suma = 0;

		for (int i = 0; i < n; i++) {
			suma += element;
			element += r;
		}
		return suma;

	}

	private static void ZliczanieZnakow() {
		String text = "Ala ma kota";
		int suma = 0;
		char znak = 'a';
		for (int i = 0; i < text.length(); i++) {

			if (text.charAt(i) == znak) {
				suma++;
			}
			;

		}
		System.out.println("Znak " + znak + " wystepuje " + suma + " razy.");
	}

	private static int MetodaZliczanieZnakow(String text, char znak) {
		int suma = 0;
		for (int i = 0; i < text.length(); i++) {

			if (text.charAt(i) == znak) {
				suma++;
			}
			;
		}
		return suma;

	}

	private static void SumaCiaguGeometr() {
		int n = 4;
		int q = 4;
		int a0 = 2;

		int element = a0;
		int suma = 0;

		for (int i = 0; i < n; i++) {

			suma += element;
			element *= q;
		}
		System.out.println(
				"Suma ciagu geometrycznego o parametrach: n= " + n + ", q=" + q + ", a0= " + a0 + " wynosi " + suma);
	}

	private static void SumaCiaguAryt() {
		int n = 4;
		int r = 4;
		int a0 = 2;

		int a = a0;
		int suma = 0;

		for (int i = 0; i < n; i++) {
			suma += a;
			a += r;
		}
		System.out.println(
				"Suma ciagu arytmetycznego o parametrach: n= " + n + ", r=" + r + ", a0= " + a0 + " wynosi " + suma);

	}

	private static int obliczSilnie(int n){
		int silnia = 1;
		for (int i = 2; i <= n; i++) {
			silnia *= i;

		}
		return silnia;
	}
	
	private static void silnia() {
		int n = 5;
		
		System.out.println("" + n + "!=" + obliczSilnie(n));
	}

	private static void iloczyn() {
		int[] array = { 1, 2, 3, 4, 5 };
		int iloczyn = 1;
		for (int i = 0; i < array.length; i++) {
			iloczyn *= array[i];
		}
		System.out.println(iloczyn);
	}

	private static void suma() {
		int[] array = { 1, 3, 2, 1, 3, 5, 2, 3 };
		int suma = 0;
		for (int i = 0; i < array.length; i++) {
			suma += array[i];
		}
		System.out.println(suma);
	}

}
