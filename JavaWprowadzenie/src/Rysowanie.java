
public class Rysowanie {

	public static void main(String[] args) {
		rysujProstokat(3,5);
		// rysujTrojkat(4);
		// rysyjPustyProstokat(4, 5);
		//dowolnalinia(3, '*');
	}

	private static void rysujProstokat(int a, int b){
		dowolnalinia(a, '*');
		System.out.println();		
		dowolnalinia(a, ' ');
		dowolnalinia(a, '*');
		
	}
	
	private static void dowolnalinia(int x, char y){
		for(int i=0; i<x; i++){
			System.out.print(y);
		}
		System.out.println();
	}

	private static void rysujTrojkat(int w) {
		for (int i = 0; i < w; i++) {
			RysujLinieTrojkata(i);
			System.out.println("*");
		}
	}

	private static void RysujLinieTrojkata(int i) {
		for (int j = 1; j <= i; j++) {
			System.out.print("*");
		}
	}

	private static void rysujProstokat(int w, int h) {
		for (int i = 1; i <= h; i++) {
			rysujLinie(w);
		}
	}

	private static void rysujLinie(int dlugoscLinii) {
		RysujLinieTrojkata(dlugoscLinii);
		System.out.println("*");
	}

}
