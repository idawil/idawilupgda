
public class RozneMetody {

	public static void main(String[] args) {
		// suma(1,2);
		// System.out.println("Iloczyn: "+iloczyn(3,2));
		// System.out.println("Najmniejsza liczba to: "+najmniejsza(4,4));

		// System.out.println("Najmniejsza "+najmniejsza(1,2,3));

		int[] liczby = { 1, 2, 1, 4, 9 };
		// System.out.println(najmniejsza(liczby));
		System.out.println(szukaj(liczby, 9));
	}

	private static int szukaj(int[] array, int liczba){
		for(int i=0; i<array.length; i++){
			if(array[i]==liczba){
				return i;
			}	
		}			
		return -1;		
	}
	

	private static int najmniejsza(int[] array) {
		if (array == null || array.length == 0) {
			throw new RuntimeException("Pusta tablica");
		}
		int wynik = array[0];
		for (int i = 1; i < array.length; i++) {
			wynik = najmniejsza(wynik, array[i]);
		}
		return wynik;
	}

	private static int najmniejsza(int a, int b, int c) {
		return najmniejsza(najmniejsza(a, b), c);
	}

	private static int najmniejsza(int a, int b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
	}

	private static void suma(int a, int b) {
		int suma = a + b;
		System.out.println("Suma: " + a + " i " + b + ": " + suma);

	}

	private static int iloczyn(int a, int b) {
		int iloczyn = a * b;
		return iloczyn;

	}

}
