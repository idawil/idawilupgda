package pl.org.pfig.carsfactory;

/**
 * Created by RENT on 2017-06-22.
 */
public class CarsFactory {
    public static CarsInterface getCar(String car){
        car=car.toLowerCase();
        switch (car){
            case "audi":
                return new Audi();
            case "volkswagen":
                return new VolksWagen();
            case "ford":
                return new Ford();
        }
        throw new IllegalArgumentException("Unknown car.");
    }
}
