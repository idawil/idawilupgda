package pl.org.pfig.singleton;

/**
 * Created by RENT on 2017-06-22.
 */
public class SingletonLifeExample {
    private static SingletonLifeExample _captain;

    private SingletonLifeExample() {
    }

    public static SingletonLifeExample getCaptain() {
        if (_captain == null) {
            System.out.println("We have just chose new captain.");
            _captain = new SingletonLifeExample();

        }
        System.out.println("We have got the best captain");
        return _captain;
    }
}