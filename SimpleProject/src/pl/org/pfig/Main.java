package pl.org.pfig;

import pl.org.pfig.carsfactory.CarsFactory;
import pl.org.pfig.carsfactory.CarsInterface;
import pl.org.pfig.factory.AnimalFactory;
import pl.org.pfig.factory.AnimalInterface;
import pl.org.pfig.factoryother.Coupon;
import pl.org.pfig.factoryother.CouponFactory;
import pl.org.pfig.fluidinterface.Beer;
import pl.org.pfig.fluidinterface.People;
import pl.org.pfig.fluidinterface.Person;
import pl.org.pfig.singleton.SingletonExample;
import pl.org.pfig.singleton.SingletonLifeExample;
import pl.org.pfig.templatemethod.Laptop;
import pl.org.pfig.templatemethod.MidiTowerComputer;
import pl.org.pfig.templatemethod.PersonalComptuter;
import pl.org.pfig.templatemethoduniversity.AppliedMathematics;
import pl.org.pfig.templatemethoduniversity.Informatics;
import pl.org.pfig.templatemethoduniversity.Management;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

     /*Singleton Patern*/
        SingletonExample se1 = SingletonExample.getInstance();
        se1.setName("pawel");

        SingletonExample se2 = SingletonExample.getInstance();
        System.out.println(se2.getName());

        SingletonLifeExample sle1 = SingletonLifeExample.getCaptain();
        SingletonLifeExample sle2 = SingletonLifeExample.getCaptain();

        /*Template Method Patern*/
        Laptop laptop = new Laptop();
        System.out.println("Laptop");
        laptop.devices();

        MidiTowerComputer mtc = new MidiTowerComputer();
        System.out.println("MidiTowerComputer");
        mtc.devices();

        PersonalComptuter pc = new PersonalComptuter();
        System.out.println("PersonalComputer");
        pc.devices();

        /*Template Method Patern University*/
        AppliedMathematics am = new AppliedMathematics();
        System.out.println("AppliedMathemiatics:");
        am.subjects();

        Informatics in = new Informatics();
        System.out.println("Informatics:");
        in.subjects();

        Management mn = new Management();
        System.out.println("Management:");
        mn.subjects();

    /*Factory Patern*/
        AnimalInterface cat = AnimalFactory.getAnimal("cat");
        cat.getSound();
        AnimalInterface dog = AnimalFactory.getAnimal("dog");
        dog.getSound();
        AnimalInterface frog = AnimalFactory.getAnimal("frog");
        frog.getSound();
        //AnimalInterface tiger=AnimalFactory.getAnimal("tiger");

        /*Factory Patern 2*/
        int[] numbers = new int[6];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = new Random().nextInt(48) + 1;

        }
        Coupon c = CouponFactory.getCoupon(numbers);
        System.out.println(c.getA());
        System.out.println(c.getB());
        System.out.println(c.getC());
        System.out.println(c.getD());
        System.out.println(c.getE());
        System.out.println(c.getF());


        /*CarsFactory pattern*/
        CarsInterface audi = CarsFactory.getCar("audi");
        System.out.print("audi ");
        audi.getModel();
        CarsInterface volkswagen = CarsFactory.getCar("volkswagen");
        System.out.print("volkswagen ");
        volkswagen.getModel();
        CarsInterface ford = CarsFactory.getCar("ford");
        System.out.print("ford ");
        ford.getModel();
        //CarsInterface mazda=CarsFactory.getCar("mazda");

        /*Beer Example chaining*/
        Beer piwo = new Beer();
        piwo
                .setName("Specjal")
                .setType("Lager")
                .setTaste("Bitter")
                .setPrice(2.19);
        System.out.println(piwo);

        /*Fluid Interface*/
        List<Person> people = new LinkedList<>();

        people.add(new Person("Pawel", "Testowy", 30));
        people.add(new Person("Pawel", "Inne", 42));
        people.add(new Person("Mirek", "Przykladowy", 22));
        people.add(new Person("Darek", "Testowy", 44));

        People pp = new People().addGroup("staff", people);
        for(Person pers: pp.from("staff").lastname("Testowy").name("Darek").get()){
            System.out.println(pers);
        }

    }
}
