package pl.org.pfig.fluidinterface;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-06-22.
 */
public class People {
    private HashMap<String, List<Person>> people=new HashMap<>();
    private List<Person> currentSearch;

    public People addGroup(String groupName, List<Person> ppl) {
        people.put(groupName, ppl);
        return this;
    }

    public People from(String from) {
        currentSearch = people.get(from);

        return this;
    }

    public People name(String name) {
        List<Person> newList= new LinkedList<>();
        for(Person p: currentSearch){
            if (p.getName().equals(name)){
                newList.add(p);
            }
        }
        currentSearch=newList;
        return this;
    }
    public People lastname(String lastname) {
        List<Person> newList= new LinkedList<>();
        for(Person p: currentSearch){
            if (p.getLastname().equals(lastname)){
                newList.add(p);
            }
        }
        currentSearch=newList;
        return this;
    }
    public List<Person> get(){

        return currentSearch;
    }


}
