package pl.org.pfig.templatemethoduniversity;

/**
 * Created by RENT on 2017-06-22.
 */
public abstract class BasicSubjects {

    public void subjects(){
        maths();
        philosophy();
        physics();
        externalSubject();
        System.out.println();
    }

    public void maths(){
        System.out.println("Maths");
    }

    public void physics(){
        System.out.println("Physics");
    }
    public void philosophy(){
        System.out.println("Philosophy");
    }

    public abstract void externalSubject();

}
