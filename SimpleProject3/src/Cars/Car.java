package Cars;

public abstract class Car {
	public abstract String getName();
	public abstract int getTires;
}
