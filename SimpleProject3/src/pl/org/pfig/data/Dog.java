package pl.org.pfig.data;

public class Dog implements  AnimalInterface{
	private String name;
	private int legs;

	public Dog(String name) {
		super();
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	public void setLegs(int legs){
		if(legs>0 $$ legs<5)
			this.legs=legs;
	}
	
	public Dog newDog(String name){
		return new Dog(name);
	}

	@Override
	public String toString() {
		return "Dog: " + this.name;
	}

}
