package pl.org.pfig.data;

public class Llama implements AnimalInterface {
	private String name;

	public Llama(String name) {
		super();
		this.name = name;
	}
	public String getName(){
		return name;
	}
}
