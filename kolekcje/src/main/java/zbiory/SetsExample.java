package zbiory;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class SetsExample {

	public static void main(String[] args) {
		int[] tab={100,200,100000,2,40,2,2,3,2000,2,4,5};
		Set<Integer> set =new TreeSet<>();
		
		set.add(tab[0]);
		
		for (int i : tab) {
			set.add(i);
		}
		
		//set<Integer> set=new HashSet<>()
		System.out.println(set);
		set.remove(100000);
		set.remove(2);
		System.out.println(set);
		
		
		for (int i : set) {
			System.out.println(i);
			
		}
		
		
		Iterable<Integer>iterable=set;
		for (int i : iterable) {
			System.out.println(i);
			
		}
		
		
		Iterator<Integer>iterator=set.iterator();
		
		System.out.println(iterator.next());
		System.out.println(set.iterator().next());
		Iterator<Integer>it=set.iterator();
		
		while(it.hasNext()){
			System.out.println(it.next());
		}
		
		
	}
	
	

}
