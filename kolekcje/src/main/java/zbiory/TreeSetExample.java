package zbiory;

import java.util.Arrays;
import java.util.TreeSet;


public class TreeSetExample {

	public static void main(String[] args) {
	
		TreeSet <Integer> treeSet=new TreeSet<>(Arrays.asList(1,2,3,4,5,6));
		
		for (int integer: treeSet) {
			System.out.println(integer);
		}
		for (Integer integer : treeSet.headSet(3)) {
			System.out.println(integer);
		}

	}

}
