package zbiory;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class StringDuplicates {
	
	
	public static boolean containDuplicates(String text) {
		
		Set<Character> set =new HashSet<>();
		
		for (char s : text.toCharArray()) {
			set.add(s);
		}
		
		return set.size()!=text.length();
		
		//if(text.length()==set.size()){
				//return false;
		//}return true;
	}

	

}
