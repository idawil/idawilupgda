package mapy;

import java.util.HashMap;
import java.util.Map;

public class MapsExamples {

	public static void main(String[] args) {
		Map<String, String> slownik = new HashMap<>();
		slownik.put("Kot", "Cat");
		slownik.put("Pies", "Dog");

		System.out.println("Warto�� kot:");
		System.out.println(slownik.get("Kot"));
	
		
		
		
		System.out.println("klucze: ");
		for (String key : slownik.keySet()) {
			System.out.println(key);
		}

		System.out.println("Warto�ci: ");
		for (String value : slownik.values()) {
			System.out.println(value);
		}
		
		
		System.out.println("Pary klucz-warto�� (encje)");
		for (Map.Entry<String, String> pair : slownik.entrySet()) {
			System.out.println(pair.getKey()+"->"+pair.getValue());
		}
		
		for (Map.Entry<String, String> pair : slownik.entrySet()) {
			System.out.println(pair);
		}
		
		slownik.remove("Pies");
		System.out.println("Pary klucz-warto�� po usuni�ciu");
		for (Map.Entry<String, String> pair : slownik.entrySet()) {
			System.out.println(pair.getKey()+"->"+pair.getValue());
		}
		System.out.println(slownik.get("Pies"));
		System.out.println(slownik.getOrDefault("Pies",	 "brak t�umaczenia"));
		
	}
}
