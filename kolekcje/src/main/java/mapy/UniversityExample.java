package mapy;

public class UniversityExample {

	public static void main(String[] args) {
		
		University uni=new University();
		uni.addStudent(1, "Ida", "Wilczek");
		uni.addStudent(2, "Anna", "Nowak");
		uni.addStudent(3, "Jan", "Kowalski");
		
		System.out.println(uni.studentExists(100200));
		System.out.println(uni.getStudent(2).getName());
		System.out.println(uni.studentsNumber());
		uni.showAll();
		
		
		
		
	}

}
