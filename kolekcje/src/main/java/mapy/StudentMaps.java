package mapy;

//import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class StudentMaps {
	
	
	public static void main(String[] args) {
		
		Map<Integer,Student> students=new TreeMap<>();
		students.put(100, new Student(100,"Jan","Kowalski"));
		students.put(101, new Student(101,"Adam","Nowak"));
		students.put(10, new Student(10,"Anna","Nowak"));
		students.put(100400, new Student(100400,"Zbigniew","Kowalski"));
		
		
		System.out.println(students.containsKey(100200));
		System.out.println(students.get(100400).getName()+" "+students.get(100400).getSurname());
		System.out.println("Liczba student�w");
		System.out.println(students.size());
		
		for (Student student : students.values()) {
			System.out.println(student.getName()+" "+student.getSurname());
		}
		
	
		
	}
	
	
	
	

}
