package kolekcje;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class PersonQueue {

	public static void main(String[] args) {
		Queue<Person> persons= new PriorityQueue<>( new Comparator<Person>() {

			@Override
			public int compare(Person o1, Person o2) {
				// TODO Auto-generated method stub
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		persons.offer(new Person("Anna",20));
		persons.offer(new Person("Adam",22));
		persons.offer(new Person("Agata",30));
		persons.offer(new Person("Pawe�",25));
		
		while(!persons.isEmpty()){
			System.out.println(persons.poll());
		}
		System.out.println(persons.poll());
			
	
	}

	}


