package kolekcje;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import kolekcje.Person;

public class SortowanieList {

	public static void main(String[] args) {
		List <Person>persons = new ArrayList<>();
		persons.add(new Person("Anna",20));
		persons.add(new Person("Adam",22));
		persons.add(new Person("Agata",30));
		persons.add(new Person("Pawe�",25));
		System.out.println("Przed sortowaniem");
		for (Person person : persons) {
			System.out.println(person);
			
		}
		Collections.sort(persons);
		System.out.println("Po sortowaniu");
		for (Person person : persons) {
			System.out.println(person);
		}
		
		Collections.shuffle(persons);
		System.out.println("Po tasowaniu");
		for (Person person : persons) {
			System.out.println(person);
		}
		
		
	}

}
