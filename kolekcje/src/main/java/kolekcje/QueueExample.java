package kolekcje;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class QueueExample {

	public static void main(String[] args) {
	Queue<Integer>queue= new PriorityQueue<>();
	queue.add(1);
	queue.add(21);
	queue.add(2);
	queue.add(10);
	queue.add(12);
	
	while(!queue.isEmpty()){
		System.out.println(queue.poll());
	}
	System.out.println(queue.poll());
	}

}
