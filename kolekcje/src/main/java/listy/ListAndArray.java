package listy;

import java.util.Arrays;
import java.util.List;

public class ListAndArray {

	public static void main(String[] args) {

		int[] array = { 4, 2, 2, 1, 5, 29, 3, 8 ,3,5};
		List<Integer> list = Arrays.asList(1, 2, 4, 2, 5, 12, 3, 2);

		System.out.println(countDuplicates(array, list));

	}

	 
	public static int countDuplicates(int[] array, List<Integer> list) {
		int count = 0;
		int l=Math.min(list.size(), array.length);
		
		for (int i = 0; i < l; i++) {
			if (list.get(i) == array[i]) {
				count++;
			}
		}

		return count;
	}

}
