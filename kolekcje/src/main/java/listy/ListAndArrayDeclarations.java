package listy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListAndArrayDeclarations {

	public static void main(String[] args) {

		tablice();
		
		List<Integer>lista=Arrays.asList(1,2,3);
		//List<Integer>lista2= new ArrayList<>(Arrays.asList(1,2,3));
		
		List<Integer>lista2=new ArrayList<>();
		lista2.add(1);
		lista2.add(2);
		System.out.println(lista2.get(1));
		
		
	}

	private static void tablice() {
		int[] tab1 = { 1, 2, 3, 4 };
		int[] tab2 = null;
		tab2 = new int[] { 1, 2 };

		tab2 = new int[2];
		tab2[0] = 1;
		tab2[1] = 2;

		int[][] tab2dim1 = { { 1, 2 }, { 1 }, { 2, 3, 5 } };
		int[][] tab2dim2 = new int[4][3];
		int[][] tab2dim3 = new int[4][];
	}
}
