package listy;

import java.util.ArrayList;
import java.util.List;

public class ListMath {
	List<Integer> numbers = new ArrayList<Integer>();
	
	
	public static int multi(List<Integer> numbers){
		int multi=1;
		for(int i=0;i<numbers.size(); i++){
			multi*=numbers.get(i);
		}
		return multi;		
	}
	
	public static int sumForEach(List<Integer> numbers){
		int sum=0;
		for(int i:numbers){
			sum+=i;
		}
		return sum;		
	}
	
	public static int sum(List<Integer> numbers){
		int sum=0;
		for(int i=0;i<numbers.size(); i++){
			sum+=numbers.get(i);
		}
		return sum;		
	}
	
	public static int average(List<Integer> numbers){
		int average=sum(numbers)/numbers.size();
	
		return average;		
	}
	
}
