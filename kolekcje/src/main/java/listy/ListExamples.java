package listy;

import java.util.ArrayList;
import java.util.List;

public class ListExamples {

	public static void main(String[] args) {
		
		List<Integer> numbers = new ArrayList<>();
		
		
		numbers.add(2);
		numbers.add(4);
		numbers.add(6);
		numbers.add(1);
		
		for (int i : numbers) {
			System.out.println(i);
		}
		
		System.out.println("Element o indeksie 2:");
		System.out.println(numbers.get(2));
		
		numbers.remove((Integer) 6);
		System.out.println("Ca�a lista:");
				for (int i : numbers) {
			System.out.println(i);
		}
				
		numbers.add(2, 100);
		System.out.println("Ca�a lista:");
		for (int i : numbers) {
	System.out.println(i);
}
	}

}
