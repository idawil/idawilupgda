package kolekcje;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import listy.ListMath;

public class ListMathTest {
@Test
public void sumForEachTest(){
	List<Integer> numbers=new ArrayList();
	numbers.add(0);
	numbers.add(5);
	numbers.add(6);
	
	assert ListMath.sum(numbers)==11;
}

@Test
public void multiTest(){
	List<Integer> numbers=new ArrayList();
	numbers.add(1);
	numbers.add(5);
	numbers.add(6);
	
	assert ListMath.multi(numbers)==30;
}
@Test
public void averageTest(){
	List<Integer> numbers=new ArrayList();
	numbers.add(1);
	numbers.add(5);
	numbers.add(6);
	
	assert ListMath.average(numbers)==4;
}

}
