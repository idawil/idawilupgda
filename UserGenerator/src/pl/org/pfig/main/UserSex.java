package pl.org.pfig.main;

public enum UserSex {
	SEX_MALE(1,"Mezczyzna"),
	SEX_FEMALE(2, "Kobieta"),
	SEX_UNDEFINED(3, "Nieokreslono");
	
	private String name;
	private int id;
	
	UserSex(int id, String name){
		this.id=id;
		this.name=name;
	}
	
	public int getId(){
		return id;
	}
	
	public String getName(){
		return name;
	}

}
