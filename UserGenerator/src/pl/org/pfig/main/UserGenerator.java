package pl.org.pfig.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {
	private String path = "resources/";
	// private User currentUser;

	public User getRandomUser() {

		UserSex us = UserSex.SEX_MALE;
		if (new Random().nextInt(2) == 0) {
			us = UserSex.SEX_FEMALE;
		}
		return getRandomUser(us);
	}

	public User getRandomUser(UserSex sex) {
		User currentUser = new User();
		currentUser.setSex(sex);
		String name = "";
		if (sex.equals(UserSex.SEX_MALE)) {
			name = getRandomLineFromFile("name_m.txt");
		} else {
			name = getRandomLineFromFile("name_f.txt");
		}
		// name and lastname
		currentUser.setName(name);
		currentUser.setSecondName(getRandomLineFromFile("lastname.txt"));
		// address
		Address ad = getRandomAddress();
		currentUser.setAddress(ad);
		// CCN
		currentUser.setCCN(getRandomCCN());
		// phone
		currentUser.setPhone(getRandomPhone());

		// birthDate
		String dt = getRandomBirthDate();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		try {
			currentUser.setBirthDate(sdf.parse(dt));
		} catch (ParseException e) {

			System.out.println(e.getMessage());
		}
		// PESEL
		currentUser.setPESEL(getRandomPESEL(currentUser));

		return currentUser;
	}

	private String getRandomPESEL(User user) {
		Date birthday = user.getBirthDate();

		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		String date = sdf.format(birthday);

		String[] dropped = date.split("");
		String day = dropped[0] + dropped[1], month = dropped[3] + dropped[4], year = dropped[8] + dropped[9];

		String p = "";

		// First 6 numbers
		int m = Integer.parseInt(month);
		int y = Integer.parseInt(date.substring(6));
		if (1799 < y && y < 1900) {
			m = m + 80;
		} else if (1999 < y && y < 2100) {
			m = m + 20;
		} else if (2099 < y && y < 2200) {
			m = m + 40;
		} else if (2199 < y && y < 2300) {
			m = m + 60;
		}

		if (m < 10) {
			month = "0" + Integer.toString(m);
		} else {
			month = Integer.toString(m);
		}

		p = p.concat(year) + p.concat(month) + p.concat(day);

		// 7-9 random
		for (int i = 0; i < 3; i++) {
			int r = new Random().nextInt(10);
			p = p.concat(Integer.toString(r));

		}

		// 10th number UserSex
		int r = new Random().nextInt(4);

		if (user.getSex() == UserSex.SEX_FEMALE) {
			String[] values = { "0", "2", "4", "6", "8" };
			p = p.concat(values[r]);
		} else {
			String[] values = { "1", "3", "5", "7", "9" };
			p = p.concat(values[r]);
		}

		// 11th number-control number
		int[] contrValue = { 9, 7, 3, 1, 9, 7, 3, 1, 9, 7 };
		String[] pes = p.split("");
		int sum = 0;
		for (int i = 0; i < 10; i++) {
			sum = sum + Integer.parseInt(pes[i]) * contrValue[i];
		}
		p = p.concat(Integer.toString(sum % 10));
		System.out.println(p);

		return p;
	}

	private String getRandomPhone() {
		String p = "";

		for (int i = 0; i < 9; i++) {
			int[] tab = new int[9];
			tab[i] = new Random().nextInt(10);
			while (tab[0] == 0) {
				tab[0] = new Random().nextInt(10);
			}
			p = p.concat(Integer.toString(tab[i]));
		}

		return p;
	}

	private String getRandomCCN() {
		String c = "";
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				int number = new Random().nextInt(10);
				c = c.concat(Integer.toString(number));
			}
			c = c.concat(" ");
		}
		return c;
	}

	private String getRandomBirthDate() {
		int[] daysInMonths = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int year = 1890 + new Random().nextInt(120);
		int month = new Random().nextInt(12) + 1;

		if (year % 4 == 0) {
			daysInMonths[1]++;
		}
		int day = new Random().nextInt(daysInMonths[month - 1]) + 1;
		return leadZero(day) + "." + leadZero(month) + "." + year;
	}

	private String leadZero(int arg) {
		if (arg < 10)
			return "0" + arg;
		else
			return "" + arg;
	}

	public Address getRandomAddress() {
		Address ad = new Address();
		String[] cityData = getRandomLineFromFile("city.txt").split(" \t ");
		ad.setCountry("Poland");
		ad.setCity(cityData[0]);
		ad.setZipcode(cityData[1]);
		ad.setStreet("ul. " + getRandomLineFromFile("street.txt"));
		ad.setNumber("" + (new Random().nextInt(100) + 1));
		return ad;

	}

	public int countLines(String filename) {
		int lines = 1;
		try (Scanner sc = new Scanner(new File(path + filename))) {
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
		} catch (FileNotFoundException e) {

			System.out.println(e.getMessage());
		}
		return lines;
	}

	public String getRandomLineFromFile(String filename) {

		int allLines = countLines(filename);
		int line = new Random().nextInt(allLines) + 1;
		int currLine = 1;
		String currentLineContent = "";
		try (Scanner sc = new Scanner(new File(path + filename))) {
			while (sc.hasNextLine()) {

				currentLineContent = sc.nextLine();
				if (line == currLine) {
					return currentLineContent;
				}
				currLine++;
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public String getRandomFile() {
		List<String> files = Arrays.asList("city.txt", "lastname.txt", "name_f.txt", "name_m.txt", "street.txt");
		Random generator = new Random();
		int i = generator.nextInt(files.size());
		return files.get(i);

	}

}
