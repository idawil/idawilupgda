var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8088/';

app.config(function($routeProvider) {
    var path = './views/';
    $routeProvider
        .when('/', {
            templateUrl: path + 'main.html'
        })  .when('/lessons', {
            templateUrl: path + 'lessons.html',
            controller: 'showController'
        }).when('/lessons/:id', {
            templateUrl: path + 'lesson.html',
            controller: 'lessonController'
        }).when('/schools', {
            templateUrl: path + 'schools.html',
            controller: 'showSchoolController'
        }).when('/schools/:id', {
            templateUrl: path + 'school.html',
            controller: 'schoolController'
        }).when('/teachers', {
            templateUrl: path + 'teachers.html',
            controller: 'showTeacherController'
        }).when('/teachers/:id', {
            templateUrl: path + 'teacher.html',
            controller: 'teacherController'
        }).when('/styles', {
            templateUrl: path + 'styles.html',
            controller: 'showStyleController'
        }).when('/styles/:id', {
            templateUrl: path + 'style.html',
            controller: 'styleController'
        }).when('/graphic', {
            templateUrl: path + 'graphics.html',
            controller: 'showGraphicController'
        }).when('/graphics/:id', {
            templateUrl: path + 'graphic.html',
            controller: 'graphicController'
        }).when('/add', {
            templateUrl: path + 'add.html',
            controller: 'addController'
        }).when('/search', {
            templateUrl: path + 'search.html',
            controller: 'searchController'
        });
});

app.controller('showController', function($scope, $http) {
    $http({
        url: url + 'lessons/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.lessons = success.data;
    }, function(error) {
        console.error(error);
    });
});

app.controller('lessonController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
   
    $http({
        url: url + 'lessons/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.lesson = success.data;
      
        
    }, function(error) {
        console.error(error);
    });
    
        
    
        $scope.add = function() {
         $http({
            url: url + 'users/add/user/'+$scope.userID+'/lesson/'+id+'/opinion/'+$scope.opinion,
            method: 'GET',
            dataType: 'json'
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.users.push(success.data);
                $scope.message = "zajęcia dodano poprawnie."
            } else
                $scope.message = "Wystąpił błąd podczas dodawania zajęć.";
        }, function(error) {
            console.error(error);
        });
    }
    
});


app.controller('schoolController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'schools/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.school = success.data;
        
    }, function(error) {
        console.error(error);
    });
});

app.controller('showSchoolController', function($scope, $http) {
    $http({
        url: url + 'schools/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.schools = success.data;
    }, function(error) {
        console.error(error);
    });
    $scope.add = function() {
         $http({
            url: url + 'schools/add',
            method: 'GET',
            dataType: 'json',
            params: {
                name: $scope.name,
                description: $scope.description,
                adress: $scope.adress        
                            
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.schools.push(success.data);
                $scope.message = "Szkołę dodano poprawnie."
            } else
                $scope.message = "Wystąpił błąd podczas dodawania szkoły.";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('teacherController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'teachers/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.teacher = success.data;
        
    }, function(error) {
        console.error(error);
    });
});

app.controller('showTeacherController', function($scope, $http) {
    $http({
        url: url + 'teachers/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.teachers = success.data;
    }, function(error) {
        console.error(error);
    });
    $scope.add = function() {
         $http({
            url: url + 'teachers/add',
            method: 'GET',
            dataType: 'json',
            params: {
                name: $scope.name,
                lastname: $scope.lastname
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.teachers.push(success.data);
                $scope.message = "Instruktora dodano poprawnie."
            } else
                $scope.message = "Wystąpił błąd podczas dodawania instruktora.";
        }, function(error) {
            console.error(error);
        });
    }
});
app.controller('styleController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'styles/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.style = success.data;
        
    }, function(error) {
        console.error(error);
    });
});

app.controller('showStyleController', function($scope, $http) {
    $http({
        url: url + 'styles/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.styles = success.data;
    }, function(error) {
        console.error(error);
    });
        $scope.add = function() {
         $http({
            url: url + 'styles/add',
            method: 'GET',
            dataType: 'json',
            params: {
                name: $scope.name,
                description: $scope.description
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.styles.push(success.data);
                $scope.message = "Styl dodano poprawnie."
            } else
                $scope.message = "Wystąpił błąd podczas dodawania.";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('graphicController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'users/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.user = success.data;
        
    }, function(error) {
        console.error(error);
    });
});

app.controller('showGraphicController', function($scope, $http) {
    $http({
        url: url + 'users/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.users = success.data;
    }, function(error) {
        console.error(error);
    });
    
        $scope.add = function() {
        $http({
            url: url + 'users/add',
            dataType: 'json',
            params: {
                name: $scope.name,
                lastname: $scope.lastname
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.users.push(success.data);
                $scope.message = "Zajęcia dodano poprawnie."
            } else
                $scope.message = "Wystąpił błąd podczas dodawania zajęć.";
        }, function(error) {
            console.error(error);
        });
    }
    
});




app.controller('addController', function($scope, $http) {
    
   $http({
        url: url + 'schools/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.schools = success.data;
    }, function(error) {
        console.error(error);
    });
        $http({
        url: url + 'teachers/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.teachers = success.data;
    }, function(error) {
        console.error(error);
    });
       $http({
        url: url + 'styles/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.styles = success.data;
    }, function(error) {
        console.error(error);
    });
    
    
    $scope.add = function() {
         $http({
            url: url + 'lessons/add',
            method: 'GET',
            dataType: 'json',
            params: {
                name: $scope.lessonName,
                description: $scope.lessonDescription,
                comments: $scope.lessonComment,
                starttime: $scope.lessonStartTime,
                endtime: $scope.lessonEndTime,                
                dayofweek: $scope.lessonDayOfWeek,
                style: $scope.lessonStyle,               
                teacher: $scope.lessonTeacher,
                school: $scope.lessonSchool
                
                
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.lessons.push(success.data);
                $scope.message = "Zajęcia dodano poprawnie."
            } else
                $scope.message = "Wystąpił błąd podczas dodawania zajęć.";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('searchController', function($scope, $http) {
    
   $http({
        url: url + 'schools/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.schools = success.data;
    }, function(error) {
        console.error(error);
    });
      
       $http({
        url: url + 'styles/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.styles = success.data;
    }, function(error) {
        console.error(error);
    });
    
    
    $scope.show = function() {
         $http({
            url: url + 'lessons/findbynamestyleschool/name/'+$scope.lessonName+'/style/'+$scope.lessonStyle+'/school/'+$scope.lessonSchool,
            method: 'GET',
            dataType: 'json',
            
        }).then(function(success) {
                $scope.lessons=success.data;
         }, function(error) {
            console.error(error);
        });
    }
});


