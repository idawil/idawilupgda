package pl.ida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.ida.entity.School;
import pl.ida.entity.SchoolAdmin;
import pl.ida.repository.SchoolAdminRepository;
import pl.ida.repository.SchoolRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/schooladmins")
public class SchoolAdminController {

    @Autowired
    private SchoolAdminRepository schoolAdminRepository;
    @Autowired
    private SchoolRepository schoolRepository;



    @RequestMapping("/")
    public String schoolAdmin() {
        return "";
    }

    @RequestMapping("/show")
    public List<SchoolAdmin> listSchoolAdmins() {

        return (List<SchoolAdmin>) schoolAdminRepository.findAll();
    }


    @RequestMapping("/add")
    public SchoolAdmin addSchoolAdmin(@RequestParam(name = "name") String name,
                              @RequestParam(name = "lastname") String lastname,
                                      @RequestParam(name="school") String school) {

        SchoolAdmin s=new SchoolAdmin();
        s.setName(name);
        s.setLastname(lastname);

        long schoolId=Long.valueOf(school);
        School mySchool=schoolRepository.findOne(schoolId);
        s.setSchool(mySchool);

        return schoolAdminRepository.save(s);
    }

    @RequestMapping("/show/{id}")
    public SchoolAdmin showSchoolAdminByID(@PathVariable("id") String id) {
        long myId = Long.valueOf(id);
        return schoolAdminRepository.findOne(myId);
    }
}
