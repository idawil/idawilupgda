package pl.ida.repository;

import org.springframework.data.repository.CrudRepository;
import pl.ida.entity.SchoolAdmin;


public interface SchoolAdminRepository extends CrudRepository<SchoolAdmin,Long> {

}
