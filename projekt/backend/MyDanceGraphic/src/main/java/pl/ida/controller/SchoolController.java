package pl.ida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.ida.entity.School;
import pl.ida.repository.SchoolRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/schools")
public class SchoolController {

    @Autowired
    private SchoolRepository schoolRepository;

    @RequestMapping("/")
    public String style() {
        return "";
    }

    @RequestMapping("/show")
    public List<School> listSchools() {

        return (List<School>) schoolRepository.findAll();
    }


    @RequestMapping("/add")
    public School addSchool(@RequestParam(name = "name") String name,
                            @RequestParam(name = "description") String description,
                            @RequestParam(name="adress")String adress) {

        School s=new School();
        s.setName(name);
        s.setDescription(description);
        s.setAdress(adress);
        return schoolRepository.save(s);
    }

    @RequestMapping("/show/{id}")
    public School showSchoolByID(@PathVariable("id") String id) {
        long myId = Long.valueOf(id);
        return schoolRepository.findOne(myId);
    }
    @RequestMapping("/showbyname/{name}")
    public School showSchoolByName(@PathVariable("name") String name) {
        return schoolRepository.findByName(name);
    }

}
