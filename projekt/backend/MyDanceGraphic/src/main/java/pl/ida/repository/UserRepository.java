package pl.ida.repository;

import org.springframework.data.repository.CrudRepository;
import pl.ida.entity.User;


public interface UserRepository extends CrudRepository<User,Long>{
}
