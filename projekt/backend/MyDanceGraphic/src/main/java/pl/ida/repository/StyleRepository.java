package pl.ida.repository;

import org.springframework.data.repository.CrudRepository;
import pl.ida.entity.Style;


public interface StyleRepository extends CrudRepository<Style,Long>{
    Style findByName(String name);
}
