package pl.ida.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@AssociationOverrides({
        @AssociationOverride(name = "pk.user",
                joinColumns = @JoinColumn(name = "user_id")),
        @AssociationOverride(name = "pk.lesson",
                joinColumns = @JoinColumn(name = "lesson_id"))})

public class UserLesson {
    @EmbeddedId
    @JsonIgnore
    private UserLessonId pk = new UserLessonId();
    private String opinion;

    public UserLesson() {
    }

    public UserLesson(UserLessonId pk, String opinion) {
        this.pk = pk;
        this.opinion = opinion;
    }

    public UserLessonId getPk() {
        return pk;
    }

    public void setPk(UserLessonId pk) {
        this.pk = pk;
    }

    public String getOpinion() {
        return opinion;
    }

    public void setOpinion(String opinion) {
        this.opinion = opinion;
    }

    public User getUser() {
        return pk.getUser();
    }

    public void setUser(User user) {
        pk.setUser(user);
    }

    public Lesson getLesson() {
        return pk.getLesson();
    }

    public void setUser(Lesson lesson) {
        pk.setLesson(lesson);
    }

}







