package pl.ida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.ida.entity.Lesson;
import pl.ida.entity.User;
import pl.ida.entity.UserLesson;
import pl.ida.entity.UserLessonId;
import pl.ida.repository.LessonRepository;
import pl.ida.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LessonRepository lessonRepository;

    @RequestMapping("/")
    public String user() {
        return "";
    }

    @RequestMapping("/show")
    public List<User> listUsers() {

        return (List<User>) userRepository.findAll();
    }


    @RequestMapping("/add")
    public User addUser(@RequestParam(name = "name") String name,
                        @RequestParam(name = "lastname") String lastname) {

        User u = new User();
        u.setName(name);
        u.setLastname(lastname);
        return userRepository.save(u);
    }

    @RequestMapping("/add/user/{userId}/lesson/{lessonId}/opinion/{opinion}")
    public User addLesson(@PathVariable("userId") long userId,
                          @PathVariable("lessonId") long lessonId,
                          @PathVariable("opinion") String opinion) {
        User user = userRepository.findOne(userId);
        Lesson lesson = lessonRepository.findOne(lessonId);
        user.getUserLessonList().add(new UserLesson(new UserLessonId(user, lesson), opinion));
        return userRepository.save(user);
    }


    @RequestMapping("/show/{id}")
    public User showUserByID(@PathVariable("id") String id) {
        long myId = Long.valueOf(id);
        return userRepository.findOne(myId);
    }

}
