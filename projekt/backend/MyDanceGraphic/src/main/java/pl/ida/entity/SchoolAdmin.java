package pl.ida.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
public class SchoolAdmin {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;
    private String name;
    private String lastname;

    @JsonManagedReference
    @ManyToOne(cascade = CascadeType.ALL)
    private School school;

    public SchoolAdmin (){}

    public SchoolAdmin(String name, String lastname,School school) {
        this.name = name;
        this.lastname = lastname;
        this.school=school;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
