package pl.ida.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class School {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String adress;
    private String description;
    @JsonBackReference
    @OneToMany(mappedBy = "school")
    private List<Lesson> lessons;

    @JsonBackReference
    @OneToMany(mappedBy = "school")
    private List<SchoolAdmin> schoolAdmins;


    public School() {

    }

    public School(String name, String adress, String description) {
        this.name = name;
        this.adress = adress;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    public List<SchoolAdmin> getSchoolAdmins() {
        return schoolAdmins;
    }

    public void setSchoolAdmins(List<SchoolAdmin> schoolAdmins) {
        this.schoolAdmins = schoolAdmins;
    }
}
