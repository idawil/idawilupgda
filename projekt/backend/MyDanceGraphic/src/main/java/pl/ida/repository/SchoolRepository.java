package pl.ida.repository;

import org.springframework.data.repository.CrudRepository;
import pl.ida.entity.School;

public interface SchoolRepository extends CrudRepository<School,Long> {
    School findByName(String name);
}
