package pl.ida.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class UserLessonId implements Serializable {
    @ManyToOne
    private User user;
    @ManyToOne
    @JsonBackReference
    private Lesson lesson;

    public UserLessonId() {
    }

    public UserLessonId(User user, Lesson lesson) {
        this.user = user;
        this.lesson = lesson;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }
}


