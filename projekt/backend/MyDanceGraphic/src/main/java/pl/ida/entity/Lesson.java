package pl.ida.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Lesson {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;
    private String comments;
    private Date startTime;
    private Date endTime;
    private String dayOfWeek;


    @JsonManagedReference
    @ManyToOne(cascade = CascadeType.ALL)
    private Style style;
    @JsonManagedReference
    @ManyToOne(cascade = CascadeType.ALL)
    private Teacher teacher;

    @JsonManagedReference
    @ManyToOne(cascade = CascadeType.ALL)
    private School school;



    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.lesson")
    private List<UserLesson> userLessonList;



    public Lesson(){

    }

    public Lesson(String name, String description, String comments, Date startTime, Date endTime,
                  String dayOfWeek, Style style, Teacher teacher, School school) {
        this.name = name;
        this.description = description;
        this.comments = comments;
        this.startTime = startTime;
        this.endTime = endTime;
        this.dayOfWeek = dayOfWeek;
        this.style=style;
        this.teacher=teacher;
        this.school=school;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

}
