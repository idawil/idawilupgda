package pl.ida.repository;

import org.springframework.data.repository.CrudRepository;
import pl.ida.entity.Teacher;


public interface TeacherRepository extends CrudRepository<Teacher,Long>{
    Teacher findByName(String name);
    Teacher findByLastname(String lastname);
}
