package pl.ida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.ida.entity.Style;
import pl.ida.repository.StyleRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/styles")
public class StyleController {
    @Autowired
     private StyleRepository styleRepository;

    @RequestMapping("/")
    public String style() {
        return "";
    }

    @RequestMapping("/show")
    public List<Style> listStyles() {

        return (List<Style>) styleRepository.findAll();
    }


    @RequestMapping("/add")
    public Style addTeacher(@RequestParam(name = "name") String name,
                            @RequestParam(name = "description") String description) {

        Style s=new Style();
        s.setName(name);
        s.setDescription(description);

        return styleRepository.save(s);
    }

    @RequestMapping("/show/{id}")
    public Style showStyleByID(@PathVariable("id") String id) {
        long myId = Long.valueOf(id);
        return styleRepository.findOne(myId);
    }
    @RequestMapping("/showbyname/{name}")
    public Style showStyleByName(@PathVariable("name") String name) {
        return styleRepository.findByName(name);
    }


}
