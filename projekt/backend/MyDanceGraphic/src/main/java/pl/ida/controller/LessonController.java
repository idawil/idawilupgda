package pl.ida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pl.ida.entity.Lesson;
import pl.ida.entity.School;
import pl.ida.entity.Style;
import pl.ida.entity.Teacher;
import pl.ida.repository.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/lessons")
public class LessonController {
    @Autowired
    private LessonRepository lessonRepository;
    @Autowired
    private StyleRepository styleRepository;
    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private SchoolRepository schoolRepository;
    @Autowired
    private UserRepository userRepository;


    @RequestMapping("/")
    public String lesson() {
        return "";
    }

    @RequestMapping("/show")
    public List<Lesson> listLessons() {

        return (List<Lesson>) lessonRepository.findAll();
    }


    @RequestMapping("/add")
    public Lesson addLesson(@RequestParam(name = "name") String name,
                            @RequestParam(name = "description") String description,
                            @RequestParam(name = "comments") String comments,
                            @RequestParam(name = "starttime") String starttime,
                            @RequestParam(name = "endtime") String endtime,
                            @RequestParam(name = "dayofweek") String dayofweek,
                            @RequestParam(name = "style") String style,
                            @RequestParam(name = "teacher") String teacher,
                            @RequestParam(name = "school") String school) {


        Style s = styleRepository.findByName(style);



        Lesson l = new Lesson();
        l.setName(name);
        l.setDescription(description);
        l.setComments(comments);

        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
        Date st = null;
        Date et = null;
        try {

            st = timeFormatter.parse(starttime);
            et = timeFormatter.parse(endtime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        st.toString();
        et.toString();

        l.setStartTime(st);
        l.setEndTime(et);
        l.setStyle(s);
        l.setDayOfWeek(dayofweek);

        long teacherId = Long.valueOf(teacher);
        Teacher t = teacherRepository.findOne(teacherId);
        l.setTeacher(t);

        long schoolId = Long.valueOf(school);
        School mySchool = schoolRepository.findOne(schoolId);
        l.setSchool(mySchool);
        return lessonRepository.save(l);
    }

    @RequestMapping("/show/{id}")
    public Lesson showLessonByID(@PathVariable("id") String id) {
        long myId = Long.valueOf(id);
        return lessonRepository.findOne(myId);
    }
    @RequestMapping("/showbyname/{name}")
    public Lesson showLessonByName(@PathVariable("name") String name) {
        return lessonRepository.findByName(name);
    }

   @RequestMapping("/findbynamestyleschool/name/{name}/style/{style}/school/{school}")
    public List<Lesson> showLessonByNameStyleSchool(@PathVariable("style") String style,
                                                    @PathVariable("name") String name,
                                                    @PathVariable("school") String school) {
    //Long st=Long.valueOf(style);
    Long st=styleRepository.findByName(style).getId();
    Long sc=Long.valueOf(school);

        return lessonRepository.findByNameAndStyleAndSchool(name,st,sc);
    }




}
