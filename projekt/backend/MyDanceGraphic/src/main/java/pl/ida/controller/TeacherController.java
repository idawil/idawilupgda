package pl.ida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.ida.entity.Teacher;
import pl.ida.entity.User;
import pl.ida.repository.TeacherRepository;
import pl.ida.repository.UserRepository;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/teachers")
public class TeacherController {
    @Autowired
    private TeacherRepository teacherRepository;



    @RequestMapping("/")
    public String teacher() {
        return "";
    }

    @RequestMapping("/show")
    public List<Teacher> listTeachers() {

        return (List<Teacher>) teacherRepository.findAll();
    }


    @RequestMapping("/add")
    public Teacher addTeacher(@RequestParam(name = "name") String name,
                        @RequestParam(name = "lastname") String lastname) {

        Teacher t=new Teacher();
        t.setName(name);
        t.setLastname(lastname);

        return teacherRepository.save(t);
    }

    @RequestMapping("/show/{id}")
    public Teacher showTeacherByID(@PathVariable("id") String id) {
        long myId = Long.valueOf(id);
        return teacherRepository.findOne(myId);
    }


}
