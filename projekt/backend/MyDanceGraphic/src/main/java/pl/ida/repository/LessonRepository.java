package pl.ida.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.ida.entity.Lesson;
import pl.ida.entity.School;
import pl.ida.entity.Style;

import java.util.List;

public interface LessonRepository extends CrudRepository<Lesson,Long>,JpaRepository<Lesson,Long> {
    Lesson findByName(String name);
   @Query(value="SELECT * FROM lesson WHERE  style_id =:lessonStyle AND school_id=:lessonSchool AND name IN " +
           "(SELECT name FROM lesson WHERE name LIKE %:lessonName%) "

            ,nativeQuery = true)
    List<Lesson> findByNameAndStyleAndSchool(@Param("lessonName") String lessonName,
                                             @Param("lessonStyle") long lessonStyle,
                                             @Param("lessonSchool") long lessonSchool
                                             );
}
