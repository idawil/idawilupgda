package pl.org.pfig.main;

import java.util.LinkedList;
import java.util.List;

import pl.org.pfig.animals.Animal;
import pl.org.pfig.animals.AnimalInterface;
import pl.org.pfig.calc.Calc;
import pl.org.pfig.calc.CalculatorInterface;
import pl.org.pfig.employee.Company;
import pl.org.pfig.employee.Employee;
import pl.org.pfig.employee.FilterInterface;

public class Main {
	public static void main(String[] args) {
		
		
		List<Employee> employers=new LinkedList<>();
		
		employers.add(new Employee("Pawel","Testowy",18,3000));
		employers.add(new Employee("Piotr","Przykladowy",32,10000));
		employers.add(new Employee("Julia","Doe",41,4300));
		employers.add(new Employee("Przemysław","Wietrak",56,4500));
		employers.add(new Employee("Zofia","Zaspa",37,3700));
		
		
		
		
		Company cc= new Company();
		cc.filter(n->n.startsWith("P"),t->t.toUpperCase());
		
		
		
		
		
		
		
		
//		String s="test";
//		System.out.println(s.startsWith("te"));
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		Thread t = new Thread(new PrzykaldowyWatek());
//
//		t.start();
//		new Thread(new InnyWatek(), "Jakis watek").start();
//		System.out.println("KONIEC");
//
//		new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				System.out.println("Pochodze z kodu Runnable");
//			}
//		}).start();
//		
//		
//		new Thread( () -> {
//			int a=1, b=3;
//			System.out.println("Suma wynosi: "+(a+b));
//		}).start();
//
//		new Thread(() -> System.out.println("Wiadomosc z FI")).start();
//		
//		
//		List<Person> p =new LinkedList<>();
//		
//		p.add(new Person(1));
//		
//		Calc c =new Calc();
//		//System.out.println(c.add(3, 4));
//		
//		System.out.println(c.oper(5,6,new CalculatorInterface() {
//			
//			@Override
//			public double doOperation(int a, int b) {				
//				return a+b;
//			}
//		}));
//		
//		System.out.println(c.oper(17, 3, (a, b)-> (a-b)));
//		System.out.println(c.oper(17, 3, (a, b)-> (a*b)));
//		
//		Animal a =new Animal();
//		a.addAnimal("zwierze1",(anims)->(anims.split(" ")));
//		a.addAnimal("pies|kot|zaba",(anims)->(anims.split("\\|")));
//		a.addAnimal("aligator_waz", (anims)->(anims.split("_")));
//		
//		a.addAnimal("katp", (anims)->{
//			String[]ret=new String[1];
//			ret[0]="";
//			for(int i=anims.length()-1;i>=0;i--){
//				ret[0]+=anims.charAt(i)+"";
//			};
//			System.out.println(ret[0]);
//			return ret;
//		});
//		
//		//System.out.println(a.containsAnimal("aligator_kot", (s)->(s.split("_"))));
//
}
}
