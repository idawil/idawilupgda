import java.util.HashMap;
import java.util.Map;

/**
 * Created by RENT on 2017-07-14.
 */
public class Request {


    public static void main(String[] args) {
        String host="test.pl";
        Map<String,String> params=new HashMap<String, String>();
        params.put("klucz","wartosc");
        params.put("k3","v3");
        System.out.println(makeRequestURI(host,params));
    }

    // http://test.pl?klucz=wartosc&k3=v3
    public static String makeRequestURI(String url, Map<String, String> params){
       String ret= url;
       ret=ret.concat("?");
        for ( String key : params.keySet()
             ) {

            ret=ret.concat(key).concat("=").concat(params.get(key)).concat("&");
        }
        ret=ret.substring(0,ret.length()-1);
        return ret;

    }
}
