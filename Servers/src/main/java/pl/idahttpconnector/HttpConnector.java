package pl.idahttpconnector;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by RENT on 2017-07-14.
 */
public class HttpConnector {


   private URL obj;
   private HttpURLConnection con;
   private String ua="Pawel/1.0";



   public String sendGET(String url) throws IOException {
       obj=new URL(url);
       con=(HttpURLConnection) obj.openConnection();
       con.setRequestMethod("GET");
       con.setRequestProperty("User-Agent",ua);
       int responseCode=con.getResponseCode();
       String result="";






       if(responseCode==200){
           InputStream res=con.getInputStream();
           InputStreamReader reader=new InputStreamReader(res);
           Scanner sc= new Scanner(reader);
           while (sc.hasNextLine()){
               result+=sc.nextLine();
           }
           sc.close();
       }
       return result;

   }
   public String sendPOST (String url, String params) throws IOException {
      url=url.concat("?login=");
       obj=new URL(url);
      con = (HttpURLConnection) obj.openConnection();
       con.setRequestMethod("POST");
       con.setRequestProperty("User-Agent", ua);


       con.setDoOutput(true);
       DataOutputStream dos = new DataOutputStream(con.getOutputStream());
       dos.writeBytes(params);
       dos.flush();
       dos.close();
       Scanner sc = new Scanner(new InputStreamReader(con.getInputStream()));
       String lines = "";
       while (sc.hasNextLine()) {
           lines += sc.nextLine();
       }
       sc.close();


return lines;
   }

}
