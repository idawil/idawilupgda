package pl.ida.zoo.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.ida.zoo.entity.Staff;
import pl.ida.zoo.util.HibernateUtil;

import java.util.List;

/**
 * Created by RENT on 2017-07-12.
 */
public class StaffDAO implements AbstractDAO<Staff>{


        public boolean insert(Staff type) {
            Session session = HibernateUtil.openSession();
            Transaction t = session.beginTransaction();
            session.save(type);
            t.commit();
            session.close();
            return true;
        }

        public boolean delete(Staff type) {
            Session session = HibernateUtil.openSession();
            Transaction t = session.beginTransaction();
            session.delete(type);
            t.commit();
            if(this.get(type.getId()) == null) {
                return true;
            }
            session.close();
            return false;
        }

        public boolean delete(int id) {
            Session session = HibernateUtil.openSession();
            Transaction t = session.beginTransaction();
            session.delete(this.get(id));
            t.commit();
            session.close();
            return true;
        }

        public boolean update(Staff type) {
            Session session = HibernateUtil.openSession();
            Transaction t = session.beginTransaction();
            session.update(type);
            t.commit();
            session.close();
            return true;
        }

        public Staff get(int id) {
            Staff staff;
            Session session = HibernateUtil.openSession();
            staff = session.load(Staff.class, id);
            session.close();
            return staff;
        }

        public List<Staff> get() {
            List<Staff> stf;
            Session session = HibernateUtil.openSession();
            stf = session.createQuery("from Staff").list();
            session.close();
            return stf;
        }
    }

