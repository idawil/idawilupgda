package pl.ida.zoo;


import pl.ida.zoo.dao.StaffDAO;
import pl.ida.zoo.entity.Staff;

/**
 * Created by RENT on 2017-07-12.
 */
public class Main {
    public static void main(String[] args) {

        StaffDAO staffDAO=new StaffDAO();
        Staff mietek=new Staff();
        mietek.setAge(13);
        mietek.setName("Mieczysław");
        mietek.setLastname("Nowak");
        staffDAO.insert(mietek);
        staffDAO.insert(new Staff("Anna","Kowalska",12));
        staffDAO.insert(new Staff("Jan","Nowak",12));
        staffDAO.insert(new Staff("Anna","Nowak",19));

        Staff slon = new Staff();
        slon = staffDAO.get(1);
        staffDAO.delete(1);
        for(Staff staff:staffDAO.get()){
            System.out.println(staff.getId()+". "+staff.getName()+" "+staff.getLastname());
        }

        Staff stf=new Staff();
        stf=staffDAO.get(2);
        stf.setLastname("ZmienioneNazwisko");
        staffDAO.update(stf);



//
//        AnimalDAO animalDAO = new AnimalDAO();
//
//        Animal a = new Animal();
//        a.setName("Słoń");
//        // BEGIN: insert
//        animalDAO.insert(a);
//        animalDAO.insert(new Animal("Orzeł"));
//        animalDAO.insert(new Animal("Gołąb"));
//
//        // BEGIN: retrieve
//        Animal slon = new Animal();
//        slon = animalDAO.get(1);
//
//        System.out.println("Pobrales id = " + slon.getId() + " " + slon.getName());
//
//        // BEGIN: update
//        slon.setName("PrzerobionySlon");
//        animalDAO.update(slon);
//
//        // BEGIN: delete
//        animalDAO.delete(3); // delete: gołąb
//
//        Animal secondAnimal = new Animal();
//        secondAnimal.setId(2);
//        animalDAO.delete(secondAnimal); // delete: orzeł
//
//        // BEGIN: retrieve all
//        for(Animal anim : animalDAO.get()) {
//            System.out.println(anim.getId() + ". " + anim.getName());
//        }
    }
}
//        Session session= HibernateUtil.openSession();
//        //BEGIN: create
//        Transaction t=session.beginTransaction();
//        Animal a=new Animal();
//        a.setName("Słoń");
//        session.save(a);
//        session.save(new Animal("Żyrafa"));
//        session.save(new Animal("Żółw"));
//        session.save(new Animal("Papuga"));
//        t.commit();
//        //END: create
//        session.close();
//        session= HibernateUtil.openSession();
//        //BEGIN retrive
//        Animal aa=null;
//        t=session.beginTransaction();
//        String query="from Animal where id=:id";
//        Query q=session.createQuery(query);
//        q.setParameter("id",1);
//        aa=(Animal) q.uniqueResult();
//        System.out.println(aa.getId() + " | " + aa.getName());
//        t.commit();
//        //END:retrive
//        session.close();
//        //BEGIN:update
//      session= HibernateUtil.openSession();
//        t=session.beginTransaction();
//        aa.setName("Krokodyl");
//        session.update(aa);
//        t.commit();
//        //END: update
//        session.close();
//        //BEGIN: delete
//        session= HibernateUtil.openSession();
//        t=session.beginTransaction();
//        Animal animalToDelete=session.load(Animal.class, new Integer(3));
//        session.delete(animalToDelete);
//        t.commit();
//        //END: delete
//        session.close();
//

//        session= HibernateUtil.openSession();
//        //BEGIN retrive all
//        List<Animal> animals=session.createQuery("from Animal ").list();
//        for (Animal anim:animals){
//            System.out.println(anim.getId()+"--"+anim.getName());
//        }
//        //END:retrive all

        //zamknięcie sesji Hibernate
//        session.close();
//
//        AnimalDAO animalDAO=new AnimalDAO();
//        for(Animal anim:animalDAO.get()){
//            System.out.println(anim.getId()+". "+anim.getName());
//        }
//        animalDAO.delete(4);
//
//    }
//}
