package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DetermineDayOfWeekTest {

	Exercises e;

	@Before

	public void init() {
		e=new Exercises();
	}

	// 1.czy zwracany jest wyjatek kiedy liczba jest poza zakresem?
	@Test
	public void whenOutOfRangeArgumentIsGivenExceptionExpected(){
		int data=-14;
		IllegalArgumentException iae=null;
		try{
			e.determineDayOfWeek(data);
		}catch(IllegalArgumentException e){
			iae=e;
		}
		
		assertNotNull("Wyjatek nie wystapil",iae);
	}

	
//	@Test(expected = IllegalArgumentException.class)
//	public void whenOutOfRangeArgumentIsGivenExceptionExpected() throws Exception {
//		
//		e.absoluteValue(8);
//	}

	// 2.czy zwracana jest poprawna wartosc dla danej liczby?

	@Test
	public void whenProperValueIsGivenProperDayResultExpected() {
		int[] data = { 1, 7 };
		String[] expected = { "niedziela", "sobota" };
		for (int i = 0; i < data.length; i++) {
			assertEquals(expected[i], e.determineDayOfWeek(data[i]));
		}

	}
	
	@Test
	public void whenProperMediumValueIsGivenProperDayResultExpected() {
		int data = 4;
		String expected = "środa";
		
			assertEquals(expected, e.determineDayOfWeek(data));
	

	}
	
	

}
