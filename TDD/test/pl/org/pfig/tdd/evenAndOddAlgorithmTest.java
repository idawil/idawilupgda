package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.lang.model.type.ArrayType;

import org.junit.Before;
import org.junit.Test;
//trening
public class evenAndOddAlgorithmTest {
	Exercises e;
	@Before
	public void init(){
		e=new Exercises();
	}
	
	
	@Test
	public void whenOutOfRangeArgumentIsGivenExceptionExpected(){
		int[][] data = {{ -3, 0},{ -5, 3}, { 25, 256}, { 0, 300}};
		IllegalArgumentException iae = null;
		for (int i = 0; i < data.length; i++) {
			try {
				e.evenAndOddAlgorithm(data[i][0], data[i][1]);
			} catch (IllegalArgumentException e) {
				iae = e;
			}
			assertNull("Wyjątek wystapił", iae);
		}
	}
	
	@Test
	public void whenFirstArgumentIsBiggerThanSecondExceptionExpected(){
		int[][] data = {{ 3, 0},{ 5, 3}, { 37, 36}, { 255, 4}};
		IllegalArgumentException iae = null;
		for (int i = 0; i < data.length; i++) {
			try {
				e.evenAndOddAlgorithm(data[i][0], data[i][1]);
			} catch (IllegalArgumentException e) {
				iae = e;
			}
			assertNull("Wyjątek wystapił", iae);
		}
	}
	
	@Test
	public void whenProperArgumentsAreGivenProperResultsExpected() {
		int[][] data = { { 3, 8}, { 5, 10}, { 3, 11} };
		
		List<List<Integer>> expected=new ArrayList<>();
		expected.add(Arrays.asList( 4, 6, 8, 7, 5, 3));
		expected.add(Arrays.asList( 6, 8, 10, 9, 7, 5));
		expected.add(Arrays.asList( 4,6,8,10,11,9,7,5,3));
		
		for (int i = 0; i < data.length; i++) {

			assertEquals(expected.get(i), e.evenAndOddAlgorithm(data[i][0], data[i][1]));

		}
	}

}
