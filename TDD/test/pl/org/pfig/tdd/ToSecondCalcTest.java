package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ToSecondCalcTest {
	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}

	@Test
	public void whenHourIsOutOfRangeArgumentIsGivenExceptionExpected() {
		int[][] data = { { -3, 0, 0 }, { -5, 3, 4 }, { 25, 3, 0 }, { 100, 6, 40 } };
		IllegalArgumentException iae = null;
		for (int i = 0; i < data.length; i++) {
			try {
				e.toSecondCalc(data[i][0], data[i][1], data[i][2]);
			} catch (IllegalArgumentException e) {
				iae = e;
			}
			assertNull("Wyjątek wystapił", iae);
		}
	}

	@Test
	public void whenMinuteIsOutOfRangeArgumentIsGivenExceptionExpected() {
		int[][] data = { { 3, -2, 0 }, { -5, -30, 4 }, { 0, 60, 3 }, { 900, 87, 40 } };
		IllegalArgumentException iae = null;
		for (int i = 0; i < data.length; i++) {
			try {
				e.toSecondCalc(data[i][0], data[i][1], data[i][2]);
			} catch (IllegalArgumentException e) {
				iae = e;
			}
			assertNull("Wyjątek wystapił", iae);
		}
	}

	@Test
	public void whenSecondIsOutOfRangeArgumentIsGivenExceptionExpected() {
		int[][] data = { { 3, 0, -4 }, { -5, -30, -70 }, { 0, 3, 70 }, { 900, 50, 40000 } };
		IllegalArgumentException iae = null;
		for (int i = 0; i < data.length; i++) {
			try {
				e.toSecondCalc(data[i][0], data[i][1], data[i][2]);
			} catch (IllegalArgumentException e) {
				iae = e;
			}
			assertNull("Wyjątek wystapił", iae);

		}
	}

	@Test
	public void whenProperArgumentsAreGivenProperResultsExpected() {
		int[][] data = { { 3, 0, 4, 1084 }, { 5, 0, 2, 1802 }, { 0, 3, 3, 183 } };

		for (int i = 0; i < data.length; i++) {

			assertEquals(data[i][3], e.toSecondCalc(data[i][0], data[i][1], data[i][2]));

		}
	}

}
