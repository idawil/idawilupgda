package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EuklidesAlgorithmTest {
	Exercises e;
	@Before
	public void init(){
		e=new Exercises();
	}
	@Test
	public void whenTwoIntegerFirstIsBiggerAreGivenProperResultExpected() {
		int[][] data= {{48,12,12},{12,3,3},{2,0,2},{4,3,1},{6,3,3}};
		for(int i=0;i<data.length; i++){
		assertEquals(data[i][2], e.euklidesAlgorithm(data[i][0], data[i][1]));}
	}
	
	
	@Test
	public void whenSeccondIsBiggerAreGivenProperResultExpected() {
		int[][] data= {{6,12,6},{3,4,1},{0,2,2},{6,210,6},{3,6,3}};
		for(int i=0;i<data.length; i++){
		assertEquals(data[i][2], e.euklidesAlgorithm(data[i][0], data[i][1]));}
	}
	@Test
	public void whenOneIntegerIsZero() {
		int[][] data= {{0,12,12},{12,0,12},{0,0,0}};
		for(int i=0;i<data.length; i++){
		assertEquals(data[i][2], e.euklidesAlgorithm(data[i][0], data[i][1]));}
	}
	
	@Test
	public void whenSecondArgumentIsNegativeExceptionExpected(){
		int data[][]={{5,-5},{3,-100},{0,-10},{90,-70}};
		
		IllegalArgumentException iae=null;
		for(int i=0;i<data.length;i++){
		try{e.euklidesAlgorithm(data[i][0], data[i][1]);
		}catch(IllegalArgumentException e){
			iae=e;
		}
		assertNotNull("Wyjatek nie wystapil",iae);}
	}
	
	@Test
	public void whenFirstArgumentIsNegativeExceptionExpected(){
int data[][]={{-5,5},{-3,-100},{-3,10},{-90,70}};
		
		IllegalArgumentException iae=null;
		for(int i=0;i<data.length;i++){
		try{e.euklidesAlgorithm(data[i][0], data[i][1]);
		}catch(IllegalArgumentException e){
			iae=e;
		}
		assertNotNull("Wyjatek nie wystapil",iae);}
	}
}
