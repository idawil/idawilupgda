package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DetermineMothTest {
	Exercises e;
	

	@Before

	public void init() {
		e=new Exercises();
	}
	@Test
	
	public void whenOutOfRangeArgumentIsGivenExceptionExpected() {
		int arg=-13;
		IllegalArgumentException iae=null;
		try{
			e.determineMonth(arg);
		}catch(IllegalArgumentException e){
			iae=e;
		}
		
		assertNotNull("Wyjatek nie wystapil",iae);
	}
	@Test
	public void whenProperValueIsGivenProperMonthResultExpected(){
		int[] data={1,12};
		String[] expected={"January","December"};
		for(int i=0; i<data.length;i++){
			assertEquals(expected[i],e.determineMonth(data[i]));
		}
	}
	
	@Test
	public void whenMediumValueIsGivenProperMonthResultExpected(){
		int data=5;
		String expected="May";
		
			assertEquals(expected,e.determineMonth(data));
		
	}
	

}
