package pl.org.pfig.tdd;

import java.util.ArrayList;
import java.util.List;

public class Exercises {

	public int absoluteValue(int num) {
		if (num < 0) {
			num = -num;
		}

		return num;

	}

	public String determineDayOfWeek(int day) throws IllegalArgumentException {
		// metoda powinna zwracac IllegalArgumentException w razie,
		// gdyby parametr przekraczal zakres <1,7>
		if (day > 7 || day < 1) {
			throw new IllegalArgumentException("Bad input value.");
		}

		String[] days = { "niedziela", "poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota" };
		return days[day - 1];

	}

	public String determineMonth(int month) {
		if (month < 1 || month > 12) {
			throw new IllegalArgumentException("Bad input value.");

		}
		String[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };
		return months[month - 1];
	}

	public int euklidesAlgorithm(int x, int y) {

		if (x < 0 || y < 0) {
			throw new IllegalArgumentException("Bad input value");
		}

		int a = Math.max(x, y);
		int b = Math.min(x, y);
		int c;
		while (b != 0) {
			c = a % b;
			a = b;
			b = c;
		}

		return a;
	}

	public int toSecondCalc(int h, int m, int s) {
		if (h < 0 || m < 0 || m > 59 || s < 0 || s > 59) {
			throw new IllegalArgumentException("Bad input value");
		}

		return h * 360 + m * 60 + s;
	}

	// Zadanie3 początki samodzielnie
	public List<Integer> evenAndOddAlgorithm(int a, int b) {
		if (a < 0 || b > 255 || a > b) {
			throw new IllegalArgumentException("Bad input value");
		}
		List<Integer> list = new ArrayList<>();

		return list;
	}

	public int[] processNumbers(int a, int b) {

		int[] ret = new int[1];

		for (int i = (a % 2 != 0) ? a + 1 : a, index = 0; i <= b; i += 2, index++) {

			System.out.println("i=" + i + " | index= " + index);
			ret[index] = i;
			ret = copyArray(ret, ret.length + 1);
		}
		for (int i = (b % 2 == 0) ? b  : b-1, index = ret.length; i >= a; i -= 2, index--) {

			System.out.println("i=" + i + " | index= " + index);
			ret[index] = i;
			ret = copyArray(ret, ret.length + 1);
		}
		return copyArray(ret, ret.length - 1);

	}

	public static int[] copyArray(int[] array, int newLen) {
		int[] ret = new int[newLen];
		int max = (newLen > array.length) ? array.length : newLen;
		for (int i = 0; i < max; i++) {
			ret[i] = array[i];
		}
		return ret;
	}

}
