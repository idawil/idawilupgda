package pl.org.pfig.tdd;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		for(int num:fillArrayWithEvenNumbersFromRange(3, 8)){
			System.out.println(num);
		}
		
		
		int[]arr={1,2,3};
		arr=Arrays.copyOf(arr,arr.length+1);
		arr[3]=123;
		for(int i:arr){
			System.out.println(i);
		}

	}
	public static int[] copyArray(int[] array,int newLen){
		int[]ret=new int[newLen];
		int max=(newLen>array.length)?array.length:newLen;
		for(int i=0; i<max;i++){
			ret[i]=array[i];
		}
		return ret;
	}
	
	//uzupełnij tablice, liczbami parzystymi <a,b>

	public static int[] fillArrayWithEvenNumbersFromRange(int a, int b) {
		int[] ret = new int[1];

		for (int i = (a % 2 != 0) ? a + 1 : a, index = 0; i <= b; i += 2, index++) {

			System.out.println("i=" + i + " | index= " + index);
			ret[index] = i;
			ret = copyArray(ret, ret.length+1);
		}
		return copyArray(ret, ret.length-1);
	}

}
